Config = Java.type('com.zlidder.Config')
PlayerHandler = Java.type('com.zlidder.world.PlayerHandler')

ContainerType = Java.type('com.zlidder.model.player.container.Container.ContainerType')

SendMessage = Java.type('com.zlidder.io.packet.outgoing.SendMessage')
ChatSettings = Java.type('com.zlidder.io.packet.outgoing.ChatSettings')
ShowSkill = Java.type('com.zlidder.io.packet.outgoing.ShowSkill')
SideBarInterface = Java.type('com.zlidder.io.packet.outgoing.SideBarInterface')
PlayerOption = Java.type('com.zlidder.io.packet.outgoing.PlayerOption')
WelcomeScreen = Java.type('com.zlidder.io.packet.outgoing.WelcomeScreen')
ShowRunEnergy = Java.type('com.zlidder.io.packet.outgoing.ShowRunEnergy')
UpdatePlayer = Java.type('com.zlidder.io.packet.outgoing.UpdatePlayer')
UpdateNpc = Java.type('com.zlidder.io.packet.outgoing.UpdateNpc')
RefreshItems = Java.type('com.zlidder.io.packet.outgoing.RefreshItems')


start = function(player) {
	// Setting the chat settings
	//player.send(new ChatSettings(0, 0, 0))
	
	// Sending Skills
	for (i = 0; i < 25; i++) {
		//player.send(new ShowSkill(i))
	}
	
	// Set sidebar interfaces
	for (i = 0; i <= SIDEBAR_INTERFACES.length; i++) {
		//player.send(new SideBarInterface(i, SIDEBAR_INTERFACES[i]))
	}
	
	// Set Player options
	//player.send(new PlayerOption(2, 0, "Attack"))

	// Open Welcome Screen
	//player.send(new WelcomeScreen())
	
	//Player/Npc update
	player.send(new UpdatePlayer())
	//player.send(new UpdateNpc())
		
	// Sending login message
	player.send(new SendMessage("Welcome to " + Config.SERVER_NAME))
	
	//player.send(new ShowRunEnergy(player.playerEnergy))
	player.updateRequired = true
	player.appearanceUpdateRequired = true
	

	//player.send(new RefreshItems(player.getInventoryContainer(), ContainerType.INVENTORY))
	//player.send(new RefreshItems(player.getBankContainer(), ContainerType.BANK))
	//player.send(new RefreshItems(player.getEquipmentContainer(), ContainerType.EQUIPMENT))
	
	player.packetManager().flushOutStream()
}

SIDEBAR_INTERFACES = [ 2423, 3917, 638, 3213, 1644, 5608, 1151, -1, 5065, 5715, 2449, 4445, 147, 6299 ]
