package com.zlidder.net.packet;

import com.zlidder.model.player.Player;

/**
 * 
 * @author fabrice
 *
 */

public abstract class OutgoingPacket {

	private final int opcode;

	public abstract void send(Player player);

	public OutgoingPacket(int opcode) {
		this.opcode = opcode;
	}

	public final int getOpcode() {
		return opcode;
	}
}