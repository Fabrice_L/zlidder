package com.zlidder.net.packet.incoming;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.world.PlayerHandler;

public class PlayerAction extends IncomingPacket {
	
	PlayerActionType actionType;

	public PlayerAction(PlayerActionType actionType) {
		this.actionType = actionType;
	}

	@Override
	public void send(Player player) {
		int attackId = player.packetManager().getInStream().readSignedWordBigEndian();
		System.out.println("Attacking " + PlayerHandler.players[attackId].getPlayerName());
	}
	
	public enum PlayerActionType {
		ATTACK,
		TRADE,
		DUEL,
		FOLLOW
	}

}
