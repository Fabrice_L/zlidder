package com.zlidder.net.packet.incoming;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.outgoing.CloseInterface;

/**
 * 
 * @author Fabrice L
 *
 */

public class Walking extends IncomingPacket {

	private WalkingType walkingType;
	
	public Walking(WalkingType walkingType) {
		this.walkingType = walkingType;
	}

	@Override
	public void send(Player player) {
		int length = player.packetManager().getPacket().getSize();
		switch(walkingType) {
		case MAP:
			length -= 14;
		case ON_COMMAND:
		case REGULAR:
			Player.newWalkCmdSteps = length - 5;
			Player.newWalkCmdSteps /= 2;
			if (++Player.newWalkCmdSteps > Player.walkingQueueSize) {
				Player.newWalkCmdSteps = 0;
				break;
			}
			int firstStepX = player.packetManager().getInStream().readSignedWordBigEndianA() - player.mapRegionX * 8;
			for (int i = 1; i < Player.newWalkCmdSteps; i++) {
				Player.newWalkCmdX[i] = player.packetManager().getInStream().readSignedByte();
				Player.newWalkCmdY[i] = player.packetManager().getInStream().readSignedByte();
			}
			Player.newWalkCmdX[0] = Player.newWalkCmdY[0] = 0;
			int firstStepY = player.packetManager().getInStream().readSignedWordBigEndian() - player.mapRegionY
					* 8;
			Player.newWalkCmdIsRunning = player.packetManager().getInStream().readSignedByteC() == 1;
			for (int i = 0; i < Player.newWalkCmdSteps; i++) {
				Player.newWalkCmdX[i] += firstStepX;
				Player.newWalkCmdY[i] += firstStepY;
			}
			player.send(new CloseInterface());
			break;		
		}
	}
	
	public enum WalkingType {
		MAP,
		ON_COMMAND,
		REGULAR
	}

}
