package com.zlidder.net.packet.incoming;

import com.zlidder.model.UpdateFlag;
import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.util.Misc;

public class ChatPacket extends IncomingPacket {

	@Override
	public void send(Player player) {
		player.chatTextEffects = player.packetManager().getInStream().readUnsignedByteS();
		player.chatTextColor = player.packetManager().getInStream().readUnsignedByteS();
		player.chatTextSize = (byte) (player.packetManager().getPacket().getSize() - 2);
		player.packetManager().getInStream().readBytes_reverseA(player.chatText, player.chatTextSize, 0);
		player.getUpdateFlags().add(UpdateFlag.CHAT);

		System.out.println("Text [" + player.chatTextEffects + "," + player.chatTextColor
				+ "]: " + Misc.textUnpack(player.chatText, player.packetManager().getPacket().getSize() - 2) + " || Name: " + player.getPlayerName());
		
	}

}
