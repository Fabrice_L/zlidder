package com.zlidder.net.packet.incoming;

import com.zlidder.model.Region;
import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.world.RegionHandler;

public class LoadRegion extends IncomingPacket {

	@Override
	public void send(Player player) {
		//player.savePlayer();
		for (Region region : RegionHandler.getRegions()) {
			if (region.getRegionX() == player.getPreviousLocation().getX()/64 
					&& region.getRegionY() == player.getPreviousLocation().getY()/64) {
				region.removeEntity(player);
			}
			if (region.getRegionX() == player.getLocation().getX()/64 
					&& region.getRegionY() == player.getLocation().getY()/64) {
				region.addEntity(player);
				player.setRegion(region);
			}
		}
	}

}
