package com.zlidder.net.packet.incoming;

import com.zlidder.model.Location;
import com.zlidder.model.object.GameObject;
import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.outgoing.SendMessage;
import com.zlidder.world.PlayerHandler;

public class ObjectAction extends IncomingPacket {
	
	private ObjectActionType actionType;

	public ObjectAction(ObjectActionType actionType) {
		this.actionType = actionType;
	}
	
	public boolean objectExists(GameObject object) {
		for (GameObject gameObject : PlayerHandler.getObjects()) {
			if (object.getId() == gameObject.getId() && object.getLocation().sameAs(gameObject.getLocation())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void send(Player player) {
		GameObject object = null;
		if (actionType == ObjectActionType.FIRST) {
			int objectX = player.packetManager().getInStream().readSignedWordBigEndianA();
			int objectId = player.packetManager().getInStream().readUnsignedWord();
			int objectY = player.packetManager().getInStream().readUnsignedWordA();
			
			object = new GameObject(objectId, new Location(objectX, objectY, player.getLocation().getHeight()));
			if (!objectExists(object)) {
				player.send(new SendMessage("No object found on this location"));
				return;
			}
			player.send(new SendMessage("You succesfully clicked the object!"));
			System.out.println("objectId: " + objectId + ", objectX: " + objectX + ", objectY: " + objectY);
		}
		if (actionType == ObjectActionType.SECOND) {
			int objectId = player.packetManager().getInStream().readUnsignedWordBigEndianA();
			int objectY = player.packetManager().getInStream().readSignedWordBigEndian();
			int objectX = player.packetManager().getInStream().readUnsignedWordA();
			System.out.println("objectId: " + objectId + ", objectX: " + objectX + ", objectY: " + objectY);
		}
		if (actionType == ObjectActionType.THIRD) {
			int objectX = player.packetManager().getInStream().readSignedWordBigEndian();
			int objectY = player.packetManager().getInStream().readUnsignedWord();
			int objectId = player.packetManager().getInStream().readUnsignedWordBigEndianA();
			System.out.println("objectId: " + objectId + ", objectX: " + objectX + ", objectY: " + objectY);
		}
	}
	
	public enum ObjectActionType {
		FIRST,
		SECOND,
		THIRD
	}

}
