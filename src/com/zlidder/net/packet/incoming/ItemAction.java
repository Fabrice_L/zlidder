package com.zlidder.net.packet.incoming;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;

/**
 * 
 * @author Fabrice L
 *
 */

public class ItemAction extends IncomingPacket {

	private ItemActionType actionType;
	
	public ItemAction(ItemActionType actionType) {
		this.actionType = actionType;
	}

	@Override
	public void send(Player player) {
		if(actionType == ItemActionType.FIRST) {
			int interfaceId = player.packetManager().getInStream().readSignedWordBigEndianA();
			int itemSlot = (player.packetManager().getInStream().readUnsignedWord() - 128);
			int itemId = (player.packetManager().getInStream().readSignedWordBigEndianA() - 128);
			System.out.println("Item: " + itemId + " from slot: "+ itemSlot + ", frame: " + interfaceId);
		}
		if(actionType == ItemActionType.SECOND) {
			int interfaceId = player.packetManager().getInStream().readSignedWordBigEndianA();
			int itemSlot = player.packetManager().getInStream().readSignedWordBigEndian();
			int itemId = player.packetManager().getInStream().readUnsignedWordA();
			System.out.println("Item: " + itemId + " from slot: " + itemSlot + ", frame: " + interfaceId);
		}
		if(actionType == ItemActionType.ITEM_ON_ITEM) {
			
		}
	}
	
	public enum ItemActionType {
		FIRST,
		SECOND,
		ITEM_ON_ITEM
	}

}
