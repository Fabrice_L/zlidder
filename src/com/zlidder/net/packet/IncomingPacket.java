package com.zlidder.net.packet;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.incoming.ChatPacket;
import com.zlidder.net.packet.incoming.ItemAction;
import com.zlidder.net.packet.incoming.LoadRegion;
import com.zlidder.net.packet.incoming.ObjectAction;
import com.zlidder.net.packet.incoming.PlayerAction;
import com.zlidder.net.packet.incoming.SilentPacket;
import com.zlidder.net.packet.incoming.Walking;
import com.zlidder.net.packet.incoming.ItemAction.ItemActionType;
import com.zlidder.net.packet.incoming.ObjectAction.ObjectActionType;
import com.zlidder.net.packet.incoming.PlayerAction.PlayerActionType;
import com.zlidder.net.packet.incoming.Walking.WalkingType;

/**
 * 
 * @author fabrice
 *
 */

public abstract class IncomingPacket {

	private static final IncomingPacket[] packets = new IncomingPacket[255];

	public abstract void send(Player player);

	public static IncomingPacket[] getPackets() {
		return packets;
	}
	
	static {
		packets[4] = new ChatPacket();
		//Item Actions
		packets[122] = new ItemAction(ItemActionType.FIRST);
		packets[75] = new ItemAction(ItemActionType.SECOND);
		packets[53] = new ItemAction(ItemActionType.ITEM_ON_ITEM);
		
		//Object Actions
		packets[132] = new ObjectAction(ObjectActionType.FIRST);
		packets[252] = new ObjectAction(ObjectActionType.SECOND);
		packets[70] = new ObjectAction(ObjectActionType.THIRD);
		
		//Player Actions
		packets[128] = new PlayerAction(PlayerActionType.ATTACK);
		packets[73] = new PlayerAction(PlayerActionType.TRADE);
		packets[139] = new PlayerAction(PlayerActionType.FOLLOW);
		packets[153] = new PlayerAction(PlayerActionType.DUEL);
		
		//Walking
		packets[248] = new Walking(WalkingType.MAP);
		packets[164] = new Walking(WalkingType.REGULAR);
		packets[98] = new Walking(WalkingType.ON_COMMAND);
				
		packets[121] = new LoadRegion();
				
		//silent packets
		packets[0] = new SilentPacket();
		packets[202] = new SilentPacket();
	}
}