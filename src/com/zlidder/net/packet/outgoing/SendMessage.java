package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

/**
 * 
 * @author Fabrice L
 *
 */

public class SendMessage extends OutgoingPacket {

	private String message;
	
	public SendMessage(String message) {
		super(253);
		this.message = message;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(message.length() + 3), player.packetManager().getOutStreamDecryption());
		out.createFrameVarSize(getOpcode());
		out.writeString(message);
		out.endFrameVarSize();
		player.send(out);
	}

}
