package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class InventoryOverlay extends OutgoingPacket {

	private int interfaceId;
	private int inventoryOverlayId;
	
	public InventoryOverlay(int interfaceId, int inventoryOverlayId) {
		super(248);
		this.interfaceId = interfaceId;
		this.inventoryOverlayId = inventoryOverlayId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(4), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWordA(interfaceId);
		out.writeWord(inventoryOverlayId);
		player.send(out);
	}

}
