package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ShowSkill extends OutgoingPacket {

	private int skillId;
	
	public ShowSkill(int skillId) {
		super(134);
		this.skillId = skillId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(7), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByte(skillId);
		player.getSkills();
		out.writeDWord_v1(player.getSkills().playerXP[skillId]);
		out.writeByte(player.getSkills().playerLevel[skillId]);
		player.send(out);
	}

}
