package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class Logout extends OutgoingPacket {

	public Logout() {
		super(109);
	}

	@Override
	public void send(Player player) {
		/*if (player.savePlayer()) {
			player.packetManager().outStream.createFrame(getOpcode());
		}*/
		Stream out = new Stream(Unpooled.buffer(1), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		player.send(out);
	}

}
