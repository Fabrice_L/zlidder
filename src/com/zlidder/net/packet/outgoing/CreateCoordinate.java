package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class CreateCoordinate extends OutgoingPacket {

	private int x;
	private int y;
	
	public CreateCoordinate(int x, int y) {
		super(85);
		this.x = x;
		this.y = y;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(3), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByteC(y - (player.mapRegionY * 8));
		out.writeByteC(x - (player.mapRegionX * 8));
		player.send(out);
	}

}
