package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ChatBoxInterface extends OutgoingPacket {

	private int interfaceId;
	
	public ChatBoxInterface(int interfaceId) {
		super(164);
		this.interfaceId = interfaceId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(3), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWordBigEndian(interfaceId);
		player.send(out);
	}

}
