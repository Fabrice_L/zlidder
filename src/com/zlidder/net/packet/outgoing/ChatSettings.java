package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ChatSettings extends OutgoingPacket {
	
	private int publicChat;
	private int privateChat;
	private int tradeBlock;

	public ChatSettings(int publicChat, int privateChat, int tradeBlock) {
		super(206);
		this.publicChat = publicChat;
		this.privateChat = privateChat;
		this.tradeBlock = tradeBlock;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(4), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByte(publicChat);
		out.writeByte(privateChat);
		out.writeByte(tradeBlock);
		player.send(out);
	}

}
