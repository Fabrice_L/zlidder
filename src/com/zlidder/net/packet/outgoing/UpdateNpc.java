package com.zlidder.net.packet.outgoing;

import com.zlidder.model.Entity;
import com.zlidder.model.Region;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.UpdatingBlock;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.Player;
import com.zlidder.model.update.UpdateEntityMovement;
import com.zlidder.model.update.UpdateEntityState;
import com.zlidder.net.PacketManager;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;
import com.zlidder.world.RegionHandler;

import io.netty.buffer.Unpooled;

public class UpdateNpc extends OutgoingPacket {

	public UpdateNpc() {
		super(65);
	}
	
	private void append(UpdatingBlock block, Stream str, Npc npc) {
		block.update(npc, str);
	}

	@Override
	public void send(Player player) {	
		
		Stream packet = new Stream(Unpooled.buffer(5000), player.packetManager().getOutStreamDecryption());
		Stream updateBlock = new Stream(Unpooled.buffer(5000), player.packetManager().getOutStreamDecryption());
		
		updateBlock.currentOffset = 0;
		
		packet.createFrameVarSizeWord(getOpcode());
		packet.initBitAccess();
		
		packet.writeBits(8, player.npcListSize);
		int size = player.npcListSize;
		player.npcListSize = 0;
		for(int i = 0; i < size; i++) {
			if(player.RebuildNPCList == false && player.getLocation().withinDistance(player.npcList[i].getLocation())) {
				append(new UpdateEntityMovement(), packet, player.npcList[i]);
				append(new UpdateEntityState(), updateBlock, player.npcList[i]);
				player.npcList[player.npcListSize++] = player.npcList[i];
			} else {
				int id = player.npcList[i].getEntityId();
				player.npcInListBitmap[id>>3] &= ~(1 << (id&7));		
				packet.writeBits(1, 1);
				packet.writeBits(2, 3);		
			}
		}

		for (Region r : RegionHandler.getRegions()) {        	
        	if (r == player.getRegion()) {
        		for (Entity entity : r.getEntities()) {
    				if(entity instanceof Npc && entity != null) {
    					int id = entity.getEntityId();
    					if (!player.RebuildNPCList && (player.npcInListBitmap[id>>3]&(1 << (id&7))) != 0) {
    						continue;
    					}
    					if (!player.getLocation().withinDistance(entity.getLocation())) {
    						continue;
    					}
    					addNewNpc((Npc) entity, player, packet, updateBlock);
    				}
        		}
        	}
        }
		
		player.RebuildNPCList = false;

		if(updateBlock.currentOffset > 0) {
			packet.writeBits(14, 16383);	
			packet.finishBitAccess();
			packet.writeBytes(updateBlock.buffer, updateBlock.currentOffset, 0);
		} else {
			packet.finishBitAccess();
		}
		packet.endFrameVarSizeWord();
		player.send(packet);
	}
	
	private void addNewNpc(Npc npc, Player player, Stream str, Stream updateBlock) {
		synchronized(this) {
			int id = npc.getEntityId();
			player.npcInListBitmap[id >> 3] |= 1 << (id&7);	
			player.npcList[player.npcListSize++] = npc;
	
			str.writeBits(14, id);	
			
			int z = npc.getLocation().getY() - player.getLocation().getY();
			if(z < 0) z += 32;
			str.writeBits(5, z);	
			z = npc.getLocation().getX() - player.getLocation().getX();
			if(z < 0) z += 32;
			str.writeBits(5, z);	
	
			str.writeBits(1, 0); 
			str.writeBits(12, npc.getNpcDefinition().getId());
			
			boolean savedUpdateRequired = npc.getUpdateFlags().contains(UpdateFlag.UPDATE);
			npc.getUpdateFlags().add(UpdateFlag.UPDATE);
			append(new UpdateEntityState(), updateBlock, npc);
			if (!savedUpdateRequired) {
				npc.getUpdateFlags().remove(UpdateFlag.UPDATE);
			}
			str.writeBits(1, 1); 
		}
	}

}
