package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ShowRunEnergy extends OutgoingPacket {

	private double runEnergy;
	
	public ShowRunEnergy(double runEnergy) {
		super(110);
		this.runEnergy = runEnergy;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(2), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByte((int) runEnergy);
		player.send(out);
	}

}
