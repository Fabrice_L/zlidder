package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ItemOnInterface extends OutgoingPacket {

	private int index;
	private int zoom;
	private int model;
	
	public ItemOnInterface(int index, int zoom, int model) {
		super(246);
		this.index = index;
		this.zoom = zoom;
		this.model = model;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(6), player.packetManager().getOutStreamDecryption());
		out.writeWordBigEndian(index);
		out.writeWord(zoom);
		out.writeWord(model);
		player.send(out);
	}

}
