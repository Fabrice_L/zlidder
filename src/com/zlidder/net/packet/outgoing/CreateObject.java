package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class CreateObject extends OutgoingPacket {

	private int objectId;
	private int objectType;
	private int objectX;
	private int objectY;
	private int objectFace;
	
	public CreateObject(int objectId, int objectType, int objectX, int objectY, int objectFace) {
		super(151);
		this.objectId = objectId;
		this.objectType = objectType;
		this.objectX = objectX;
		this.objectY = objectY;
		this.objectFace = objectFace;
	}

	@Override
	public void send(Player player) {
		
		player.send(new CreateCoordinate(objectX, objectY));
		
		Stream out = new Stream(Unpooled.buffer(5), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByteS(0);
		out.writeWordBigEndian(objectId);
		out.writeByteS((objectType << 2) + (objectFace & 3));
		player.send(out);

	}

}
