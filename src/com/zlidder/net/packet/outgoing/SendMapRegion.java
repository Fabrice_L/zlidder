package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class SendMapRegion extends OutgoingPacket {

	public SendMapRegion() {
		super(73);
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(5), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWordA(player.mapRegionX + 6);
		out.writeWord(player.mapRegionY + 6);
		player.send(out);
	}

}
