package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class SendString extends OutgoingPacket {

	private String message;
	private int widgetId;
	
	public SendString(String message, int widgetId) {
		super(126);
		this.message = message;
		this.widgetId = widgetId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(message.length() + 3), player.packetManager().getOutStreamDecryption());
		out.createFrameVarSizeWord(getOpcode());
		out.writeString(message);
		out.writeWordA(widgetId);
		out.endFrameVarSizeWord();
		player.send(out);
	}

}
