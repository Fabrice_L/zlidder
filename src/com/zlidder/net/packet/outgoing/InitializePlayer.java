package com.zlidder.net.packet.outgoing;

import com.zlidder.Config;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.model.player.container.Container.ContainerType;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.ISAACCipher;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class InitializePlayer extends OutgoingPacket {

	public InitializePlayer() {
		super(249);
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(4), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByteA(0); // 1 for members, zero for free
		out.writeWordBigEndianA(player.getEntityId());
		player.send(out);
		
		player.getUpdateFlags().add(UpdateFlag.UPDATE);
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		
		//ScriptManager.runScript("on_login", player);
		
		initialize(player);
	}
	
	public void initialize(Player player) {
		// Setting the chat settings
		player.send(new ChatSettings(0, 0, 0));
		
		// Sending Skills
		for (int i = 0; i < 25; i++) {
			player.send(new ShowSkill(i));
		}
		
		// Set sidebar interfaces
		for (int i = 0; i < SIDEBAR_INTERFACES.length; i++) {
			player.send(new SideBarInterface(i, SIDEBAR_INTERFACES[i]));
		}
		
		// Set Player options
		player.send(new PlayerOption(2, 0, "Attack"));

		// Open Welcome Screen
		player.send(new WelcomeScreen());
		
		//Player/Npc update
		player.send(new UpdatePlayer());
		player.send(new UpdateNpc());
			
		// Sending login message
		player.send(new SendMessage("Welcome to " + Config.SERVER_NAME));
		
		player.send(new ShowRunEnergy(player.playerEnergy));
		player.getUpdateFlags().add(UpdateFlag.UPDATE);
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		
		player.send(new RefreshItems(player.getInventoryContainer(), ContainerType.INVENTORY));
		player.send(new RefreshItems(player.getBankContainer(), ContainerType.BANK));
		player.send(new RefreshItems(player.getEquipmentContainer(), ContainerType.EQUIPMENT));
	}
	int[] SIDEBAR_INTERFACES = { 2423, 3917, 638, 3213, 1644, 5608, 1151, -1, 5065, 5715, 2449, 4445, 147, 6299 };

}
