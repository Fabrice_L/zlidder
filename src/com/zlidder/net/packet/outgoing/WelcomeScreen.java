package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class WelcomeScreen extends OutgoingPacket {

	public WelcomeScreen() {
		super(176);
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(11), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByteC(201);
		out.writeWordA(0); //unread messages
		out.writeByte(0);
		out.writeDWord_v2(-1); // ip of last login
		out.writeWord(0); // days
		player.send(out);
	}

}
