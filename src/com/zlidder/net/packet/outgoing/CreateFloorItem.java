package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class CreateFloorItem extends OutgoingPacket {

	private int itemId;
	private int itemAmount;
	private int itemX;
	private int itemY;
	
	public CreateFloorItem(int itemId, int itemAmount, int itemX, int itemY) {
		super(44);
		this.itemId = itemId;
		this.itemAmount = itemAmount;
		this.itemX = itemX;
		this.itemY = itemY;
	}

	@Override
	public void send(Player player) {
		player.send(new CreateCoordinate(itemX, itemY));
		
		Stream out = new Stream(Unpooled.buffer(6), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWordBigEndianA(itemId);
		out.writeWord(itemAmount);
		out.writeByte(0);
		player.send(out);
	}

}
