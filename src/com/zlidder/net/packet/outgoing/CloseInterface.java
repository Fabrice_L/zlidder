package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class CloseInterface extends OutgoingPacket {

	public CloseInterface() {
		super(219);
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(1), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		player.send(out);
	}

}
