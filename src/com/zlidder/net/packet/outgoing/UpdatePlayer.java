package com.zlidder.net.packet.outgoing;

import com.zlidder.model.Entity;
import com.zlidder.model.Region;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.UpdatingBlock;
import com.zlidder.model.player.Player;
import com.zlidder.model.update.UpdateEntityMovement;
import com.zlidder.model.update.UpdateEntityState;
import com.zlidder.model.update.UpdateThisEntityMovement;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;
import com.zlidder.world.RegionHandler;

import io.netty.buffer.Unpooled;

public class UpdatePlayer extends OutgoingPacket {

	public UpdatePlayer() {
		super(81);
	}
	
	private void append(UpdatingBlock block, Stream str, Player player) {
		block.update(player, str);
	}

	@Override
	public void send(Player player) {
		if (player.getUpdateFlags().contains(UpdateFlag.MAPREGION)) {
			player.send(new SendMapRegion());
		}
		
		Stream packet = new Stream(Unpooled.buffer(5000), player.packetManager().getOutStreamDecryption());
		Stream updateBlock = new Stream(Unpooled.buffer(5000), player.packetManager().getOutStreamDecryption());
		updateBlock.currentOffset = 0;

		append(new UpdateThisEntityMovement(), packet, player);
		
		boolean saveChatTextUpdate = player.getUpdateFlags().contains(UpdateFlag.CHAT);
		player.getUpdateFlags().remove(UpdateFlag.CHAT);
		append(new UpdateEntityState(), updateBlock, player);
		
		if (saveChatTextUpdate) {
			player.getUpdateFlags().add(UpdateFlag.CHAT);
		}

		if(player.initialized){
			packet.writeBits(8, player.playerListSize);
			int size = player.playerListSize;	
			player.playersUpdating.clear();
			player.playerListSize = 0;	
			for(int i = 0; i < size; i++) {
				if(player.playerList[i] != null && player.playerList[i].initialized && !player.playerList[i].getUpdateFlags().contains(UpdateFlag.TELEPORT) && !player.getUpdateFlags().contains(UpdateFlag.TELEPORT) && player.getLocation().withinDistance(player.playerList[i].getLocation())) {
					append(new UpdateEntityMovement(), packet, player.playerList[i]);
					append(new UpdateEntityState(), updateBlock, player.playerList[i]);
					player.playerList[player.playerListSize++] = player.playerList[i];
					player.playersUpdating.add(player.playerList[i]); //Danno: So we don't add them in the next loop.
					
				} else {
					player.playersUpdating.add(player.playerList[i]); 
					/**
					 * Danno: If they move in and out inbetween these two loops
					 * 		  they won't be re-added, which might cause client dc's.
					 */
					packet.writeBits(1, 1);
					packet.writeBits(2, 3);
				}
			}
			
	        for (Region r : RegionHandler.getRegions()) {        	
	        	if (r == player.getRegion()) {
	        		for (Entity entity : r.getEntities()) {
	        			if (entity instanceof Player && entity != null && ((Player)entity).isActive && entity != player) {
	        				if (!player.playersUpdating.contains(entity) && player.getLocation().withinDistance(entity.getLocation())) {
	        					addNewPlayer((Player) entity, player, packet, updateBlock);
	        					System.out.println("showing player: " + ((Player)entity).getPlayerName());
	        				}
	        			}
	        		}
	        	}
	        }
		} else {
			packet.writeBits(8, 0);
			
		}

		if(updateBlock.currentOffset > 0) {
			packet.writeBits(11, 2047);	// magic EOF - needed only when player updateblock follows
			packet.finishBitAccess();

			// append update block
			packet.writeBytes(updateBlock.buffer, updateBlock.currentOffset, 0);
		} else {
			packet.finishBitAccess();
		}
		packet.endFrameVarSizeWord();
		player.send(packet);
	}
	
	private void addNewPlayer(Player plr, Player player, Stream str, Stream updateBlock) {
		int id = plr.getEntityId();
		player.playerList[player.playerListSize++] = plr;
		str.writeBits(11, id);
		str.writeBits(1, 1);
		boolean savedFlag = plr.getUpdateFlags().contains(UpdateFlag.APPEARANCE);
		boolean savedUpdateRequired = plr.getUpdateFlags().contains(UpdateFlag.UPDATE);
		plr.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		plr.getUpdateFlags().add(UpdateFlag.UPDATE);
		append(new UpdateEntityState(), updateBlock, plr);
		if (savedFlag && !plr.getUpdateFlags().contains(UpdateFlag.APPEARANCE)) {
			plr.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		}
		if (savedUpdateRequired && !plr.getUpdateFlags().contains(UpdateFlag.UPDATE)) {
			plr.getUpdateFlags().add(UpdateFlag.UPDATE);
		}
		str.writeBits(1, 1);
		int z = plr.getLocation().getY() - player.getLocation().getY();
		if (z < 0)
			z += 32;
		str.writeBits(5, z);
		z = plr.getLocation().getX() - player.getLocation().getX();
		if (z < 0)
			z += 32;
		str.writeBits(5, z);
	}

}
