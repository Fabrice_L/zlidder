package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class ShowInterface extends OutgoingPacket {

	private int interfaceId;
	
	public ShowInterface(int interfaceId) {
		super(97);
		this.interfaceId = interfaceId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(2), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWord(interfaceId);
		player.send(out);
	}

}
