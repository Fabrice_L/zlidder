package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class RemoveFloorItem extends OutgoingPacket {

	private int itemId;
	private int itemX;
	private int itemY;
	
	public RemoveFloorItem(int itemId, int itemX, int itemY) {
		super(156);
		this.itemId = itemId;
		this.itemX = itemX;
		this.itemY = itemY;
	}

	@Override
	public void send(Player player) {
		player.send(new CreateCoordinate(itemX, itemY));
		
		Stream out = new Stream(Unpooled.buffer(6), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeByteS(0);
		out.writeWord(itemId);
		player.send(out);		
	}

}
