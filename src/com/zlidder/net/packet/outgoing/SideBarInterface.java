package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class SideBarInterface extends OutgoingPacket {
	
	private int tabId;
	private int interfaceId;

	public SideBarInterface(int tabId, int interfaceId) {
		super(71);
		this.tabId = tabId;
		this.interfaceId = interfaceId;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(4), player.packetManager().getOutStreamDecryption());
		out.createFrame(getOpcode());
		out.writeWord(interfaceId);
		out.writeByteA(tabId);
		player.send(out);
	}

}
