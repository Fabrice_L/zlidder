package com.zlidder.net.packet.outgoing;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class PlayerOption extends OutgoingPacket {
	
	private int optionPosition;
	private int topFlag;
	private String optionText;

	public PlayerOption(int optionPosition, int topFlag, String optionText) {
		super(104);
		this.optionPosition = optionPosition;
		this.topFlag = topFlag;
		this.optionText = optionText;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(optionText.length() + 5), player.packetManager().getOutStreamDecryption());
		out.createFrameVarSize(getOpcode());
		out.writeByteC(optionPosition);
		out.writeByteA(topFlag);
		out.writeString(optionText);
		out.endFrameVarSize();
		player.send(out);
	}

}
