package com.zlidder.net.packet.outgoing;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.model.player.container.Container;
import com.zlidder.model.player.container.Container.ContainerType;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.util.Stream;

import io.netty.buffer.Unpooled;

public class RefreshItems extends OutgoingPacket {

	private Container container;
	private ContainerType containerType;
	
	public RefreshItems(Container container, ContainerType containerType) {
		super(53);
		this.container = container;
		this.containerType = containerType;
	}

	@Override
	public void send(Player player) {
		Stream out = new Stream(Unpooled.buffer(950), player.packetManager().getOutStreamDecryption());
		out.createFrameVarSizeWord(getOpcode()); 
		out.writeWord(containerType.getMoveWindow());
		out.writeWord(container.getCapacity());
		for (int i = 0; i < container.getCapacity(); i++) {
			if (container.getItems()[i].getAmount() > 254) {
				out.writeByte(255);
				out.writeDWord_v2(container.getItems()[i].getAmount());
			} else {
				out.writeByte(container.getItems()[i].getAmount());
			}
			if (container.getItems()[i].getId() > 6540 || container.getItems()[i].getId() < 0) {
				container.getItems()[i] = new GameItem(6540, container.getItems()[i].getAmount());
			}
			out.writeWordBigEndianA(container.getItems()[i].getId() < 1 ? 0 : container.getItems()[i].getId()+1);
		}
		out.endFrameVarSizeWord();
		player.send(out);
	}

}
