package com.zlidder.net;

import com.zlidder.model.player.Player;
import com.zlidder.world.PlayerHandler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ChannelHandler extends SimpleChannelInboundHandler<Object> {

	private Player player = null;

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
		if (!ctx.channel().isActive()) {
			return;
		}
		if (msg instanceof Player) {
			player = (Player) msg;
			PlayerHandler.queueLogin(player);
		} else if (msg instanceof Packet) {
			player.packetManager().setPacket((Packet)msg);
			player.packetManager().queuePacket((Packet)msg);
		}
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		if(player != null) {
			player.destruct();
			player = null;
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.channel().close();
	}

}