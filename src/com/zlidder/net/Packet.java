package com.zlidder.net;

import io.netty.buffer.ByteBuf;

public class Packet {
	
	private final int opcode;
	private final int size;
	private final ByteBuf payload;
	
	public Packet(int opcode, int size, ByteBuf payload) {
		this.opcode = opcode;
		this.size = size;
		this.payload = payload;
	}

	public int getOpcode() {
		return opcode;
	}

	public int getSize() {
		return size;
	}

	public ByteBuf getPayload() {
		return payload;
	}

}