package com.zlidder.net;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.zlidder.model.player.Player;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.util.ISAACCipher;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;

public class PacketManager {

	private Player player;
	
	private Queue<Packet> queuedPackets = new ConcurrentLinkedQueue<Packet>();
	
	private int timeOutCounter;
	
	private Stream inStream;
	private Packet packet;

	private ISAACCipher inStreamDecryption = null;
	private ISAACCipher outStreamDecryption = null;
	
	public PacketManager(Player player) {
		this.setPlayer(player);
	}
	public void queuePacket(Packet packet) {
		queuedPackets.add(packet);
	}
	public boolean processQueuedPackets() {
		Packet packets = null;
		while((packets = queuedPackets.poll()) != null) {
			//handlePacket(packet.getOpcode(), packet.getSize(), StreamBuffer.OutBuffer.newInBuffer(packet.getPayload()));
			packet = packets;
			inStream = new Stream(packet.getPayload().array());
			if (inStream == null) {
				return false;
			}
			player.send(IncomingPacket.getPackets()[packet.getOpcode()]);
			player.parseIncomingPackets();

		}
		return true;
	}
	
	public boolean packetProcess() {
	    try {
	      if (timeOutCounter > 20) {
	        System.out.println("Disconnected " + player.getPlayerName() + ", data transfer timeout.");
	        player.disconnected = true;
	        return false;
	      }
	      timeOutCounter++;
	      
	      if (getPacket() == null) {
	    	  return false;
	      }
	      
	      inStream = new Stream(getPacket().getPayload().array());
	      if (inStream == null) {
	    	  return false;
	      }
	      
	      player.send(IncomingPacket.getPackets()[getPacket().getOpcode()]);
	      player.parseIncomingPackets();
	      timeOutCounter = 0;
	      setPacket(null);
	    } catch (java.lang.Exception __ex) {
	      Misc.println("Exception encountered while parsing incoming packets from " + player.getPlayerName() + ".");
	      __ex.printStackTrace();
	      player.disconnected = true;
	    }
	    return true;
	  }

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public ISAACCipher getInStreamDecryption() {
		return inStreamDecryption;
	}

	public void setInStreamDecryption(ISAACCipher inStreamDecryption) {
		this.inStreamDecryption = inStreamDecryption;
	}

	public ISAACCipher getOutStreamDecryption() {
		return outStreamDecryption;
	}

	public void setOutStreamDecryption(ISAACCipher outStreamDecryption) {
		this.outStreamDecryption = outStreamDecryption;
	}

	public Stream getInStream() {
		return inStream;
	}

	public void setInStream(Stream inStream) {
		this.inStream = inStream;
	}

	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}

}
