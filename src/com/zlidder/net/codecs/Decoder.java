package com.zlidder.net.codecs;

import java.util.List;

import com.zlidder.Config;
import com.zlidder.net.Packet;
import com.zlidder.util.ISAACCipher;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class Decoder extends ByteToMessageDecoder {

	private final ISAACCipher cipher;
	private int opcode = -1;
	private int size = -1;
	
	public Decoder(ISAACCipher cipher) {
		this.cipher = cipher;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> out) throws Exception {
		if (opcode == -1) {
			if (buffer.readableBytes() >= 1) {
				opcode = buffer.readByte() & 0xFF;
				opcode = (opcode - cipher.getNextValue()) & 0xFF;
				size = Config.PACKET_SIZES[opcode];
			}
		}
		if (size == -1) {
			if (buffer.readableBytes() >= 1) {
				size = buffer.readByte() & 0xFF;
			}
		}
		if (buffer.readableBytes() >= size) {
			byte[] data = new byte[size];
			buffer.readBytes(data);
			
			ByteBuf payload = Unpooled.buffer(size);
			payload.writeBytes(data);
			
			try {
				out.add(new Packet(opcode, size, payload));
			} finally {
				opcode = -1;
				size = -1;
			}
		}
	}

}