package com.zlidder.net.codecs;

import java.security.SecureRandom;
import java.util.List;

import com.zlidder.model.player.Player;
import com.zlidder.net.PacketManager;
import com.zlidder.util.ISAACCipher;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;
import com.zlidder.world.PlayerHandler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class LoginDecoder extends ByteToMessageDecoder {

	private static enum State {
		LOGIN_START,
		LOGIN_READ1,
		LOGIN_READ2,
	}
	
	private State state = State.LOGIN_START;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if(state == State.LOGIN_START) {
			if (in.readableBytes() < 2) {
				return;
			}
			
			int request = in.readUnsignedByte();
			if(request != 14) {
				ctx.channel().close();
				throw new Exception("Expect login byte 14 from Client.");
			}
			
			//Name part
			in.readUnsignedByte();
			
			ByteBuf outBuf = Unpooled.buffer(17);
			outBuf.writeLong(0); // First 8 bytes are ignored by the client.
			outBuf.writeByte(0); // The response opcode, 0 for logging in.
			outBuf.writeLong(new SecureRandom().nextLong()); // SSK.
			//out.add(outBuf);
			ctx.channel().write(outBuf);
			state = State.LOGIN_READ1;
			
		} else if(state == State.LOGIN_READ1) {
			if (in.readableBytes() < 2) {
				return;
			}
			
			int loginType = in.readUnsignedByte();
			if (loginType != 16 && loginType != 18) {
				System.out.println("Invalid login type: " + loginType);
				ctx.channel().close();
				return;
			}
			
			int blockLength = in.readUnsignedByte();
			int loginEncryptSize = blockLength - (36 + 1 + 1 + 2);
			if (loginEncryptSize <= 0) {
				System.out.println("Encrypted packet size zero or negative: " + loginEncryptSize);
				ctx.channel().close();
				return;
			}
			
			in.readUnsignedByte(); // Magic id

			int clientVersion = in.readUnsignedShort();
			if (clientVersion != 317) {
				System.out.println("Invalid client version: " + clientVersion);
				ctx.channel().close();
				return;
			}
			
			// High/low memory
			in.readByte();

			// Skip the CRC keys.
			for (int i = 0; i < 9; i++)
				in.readInt();
			
			// Skip RSA block length.
			in.readByte();

			// Validate that the RSA block was decoded properly.
			int rsaOpcode = in.readByte();
			if (rsaOpcode != 10) {
				System.err.println("Unable to decode RSA block properly!");
				ctx.channel().close();
				return;
			}
			
			// Set up the ISAAC ciphers.
			long clientHalf = in.readLong();
			long serverHalf = in.readLong();
			int[] isaacSeed = { (int) (clientHalf >> 32), (int) clientHalf, (int) (serverHalf >> 32), (int) serverHalf };
			ISAACCipher inCipher = new ISAACCipher(isaacSeed);
			for (int i = 0; i < isaacSeed.length; i++)
				isaacSeed[i] += 50;
			ISAACCipher outCipher = new ISAACCipher(isaacSeed);
			
			int version = in.readInt();
			
			String username = Misc.getRS2String(in);
			String password = Misc.getRS2String(in);
			out.add(login(ctx.channel(), inCipher, outCipher, version, username, password));

		}
		
	}

	private static Player login(Channel channel, ISAACCipher inCipher, ISAACCipher outCipher, int version, String username,
			String password) {
		// Validate name
		if (!username.matches("[A-Za-z0-9 ]+") || username.length() > 12 || username.length() <= 0) {
			sendReturnCode(channel, 16);
			return null;
		}
	        
		// Switch the packet decoder to the game decoder
		channel.pipeline().remove("decoder");
		channel.pipeline().addFirst("decoder", new Decoder(inCipher));
		int slot = PlayerHandler.getFreeSlot();

		Player player = new Player(channel, slot);
		player.setPlayerName(username);
		player.setPassword(password);
		
		player.packetManager().setOutStreamDecryption(outCipher);
        
		return player;
		
	}
	

	public static void sendReturnCode(Channel channel, int code) {
		// Write the response.
		ByteBuf outBuf = Unpooled.buffer(1);
		outBuf.writeByte(code); // First 8 bytes are ignored by the client.
		
		channel.write(outBuf).addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(final ChannelFuture arg0) throws Exception {
				arg0.channel().close();
			}
		});
	}

}
