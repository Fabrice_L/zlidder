package com.zlidder.model.item;

import com.zlidder.io.cache.def.ItemDefinition;

public class GameItem {

	/**
	 * The item id.
	 */
	private int id;

	/**
	 * The item amount.
	 */
	private int amount;

	/**
	 * Creates a new item.
	 * 
	 * @param id
	 *            The item id.
	 * @param amount
	 *            The item amount.
	 */
	public GameItem(int id, int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("Count cannot be negative.");
		}
		this.id = id;
		this.amount = amount;
	}

	/**
	 * Gets the item id.
	 * 
	 * @return The item id.
	 */
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the item amount.
	 * 
	 * @return The item amount.
	 */
	public int getAmount() {
		return amount;
	}
	
	public int addAmount(int amount) {
		return this.amount += amount;
	}
	
	public int removeAmount(int amount) {
		return this.amount -= amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public boolean isStackable() {
		return ItemDefinition.lookup(getId()).isStackable();
	}
	
	public String getName() {
		return ItemDefinition.lookup(getId()).getName();
	}
	
	public int noteToItem() {
		return ItemDefinition.noteToItem(getId());
	}
	
	public int itemToNote() {
		return ItemDefinition.itemToNote(getId());
	}
	
	public boolean isNoted() {
		return ItemDefinition.lookup(getId()).isNote();
	}
}