package com.zlidder.model;

import java.util.LinkedList;
import java.util.List;

public class Region {

	private List<Entity> entities = new LinkedList<Entity>();
	
	private int regionX;
	private int regionY;
	
	public Region(int regionX, int regionY) {
		this.regionX = regionX;
		this.regionY = regionY;
	}
	
	public List<Entity> getEntities() {
		return entities;
	}
	
	public void addEntity(Entity entity) {
		getEntities().add(entity);
	}
	
	public void removeEntity(Entity entity) {
		getEntities().remove(entity);
	}

	public int getRegionX() {
		return regionX;
	}

	public void setRegionX(int regionX) {
		this.regionX = regionX;
	}

	public int getRegionY() {
		return regionY;
	}

	public void setRegionY(int regionY) {
		this.regionY = regionY;
	}
	
}
