package com.zlidder.model;

import java.util.LinkedList;
import java.util.List;

import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.Player;
import com.zlidder.util.Stream;

public abstract class Entity {
		
	private EntityAttribute attributes = new EntityAttribute();
	private EntityUpdateFlag updateFlags = new EntityUpdateFlag();
	private List<Entity> localEntities = new LinkedList<Entity>();
	
	
	private Location nextLocation;
	private Location previousLocation;
	private Location location;
	private Region region;
	
	private int entityId;
	
	private int primaryDirection;
	private int secondaryDirection;
	
	private int animationId;
	private int animationDelay;
	private int graphicId;
	private int graphicDelay;
	private int faceId;
	
	private boolean poisoned;
	private boolean isDead;
	private int hitDiff;
	private int currentHitpoints;
	private int maxHitpoints;

	public void appendHitUpdate(Stream str) {
		if (this instanceof Player) {
			str.writeByte(getHitDiff()); // What the perseon got 'hit' for
			if (getHitDiff() > 0 && !poisoned) {
				str.writeByteS(1); // 0: red hitting - 1: blue hitting
			} else if (getHitDiff() > 0 && poisoned) {
				str.writeByteS(2); // 0: red hitting - 1: blue hitting 2: poison
									// 3: orange
			}else {
				str.writeByteS(0); // 0: red hitting - 1: blue hitting
			}
			setCurrentHitpoints((((Player)this).getSkills().playerLevel[3] - getHitDiff()));
			if (getCurrentHitpoints() <= 0) {
				setCurrentHitpoints(0);
				setDead(true);
			}
			str.writeByte(getCurrentHitpoints());
			str.writeByteC(((Player)this).getSkills().getLevelForXP(3));
			poisoned = false;
		} else {
			if (getCurrentHitpoints() <= 0) {
				setDead(true);
			}
			str.writeByteC(getHitDiff()); 
			if (getHitDiff() > 0) {
				str.writeByteS(1); 
			} else {
				str.writeByteS(0); 
			}
			str.writeByteS(getCurrentHitpoints()); 
			str.writeByteC(getMaxHitpoints());
			System.out.println(getCurrentHitpoints() + "/" + getMaxHitpoints());
		}
	}
	
	public void faceEntity(Entity entity) {
		if (entity instanceof Player) {
			faceId = 32768 + entity.entityId;
		} else if (entity instanceof Npc) {
			faceId = entity.entityId;
		} else {
			faceId = 65535;
		}
		getUpdateFlags().add(UpdateFlag.FACE);
		getUpdateFlags().add(UpdateFlag.UPDATE);
	}
	
	public void appendFaceEntity(Stream str) {
		if (this instanceof Player) {
			str.writeWordBigEndian(faceId);
		} else { 
			str.writeWord(faceId);
		}
	}
	
	public void playGraphic(int id, int delay) {
		graphicId = id;
		graphicDelay = delay;
		getUpdateFlags().add(UpdateFlag.GRAPHIC);
		getUpdateFlags().add(UpdateFlag.UPDATE);
	}
	
	public void appendGraphicUpdate(Stream str) {
		if (this instanceof Player) {
			str.writeWordBigEndian(graphicId);
			str.writeDWord(graphicDelay);
		} else {
			str.writeWord(graphicId);
		    str.writeDWord(graphicDelay);
		}
		getUpdateFlags().remove(UpdateFlag.GRAPHIC);
	}
	
	public void playAnimation(int id, int delay) {
		animationId = id;
		animationDelay = delay;
		getUpdateFlags().add(UpdateFlag.ANIMATION);
		getUpdateFlags().add(UpdateFlag.UPDATE);
	}
	
	public void appendAnimUpdate(Stream str) {
		str.writeWordBigEndian(animationId);
		str.writeByte(animationDelay);
		//getUpdateFlags().remove(UpdateFlag.ANIMATION);
	}

	public EntityAttribute getAttributes() {
		return attributes;
	}
	
	public EntityUpdateFlag getUpdateFlags() {
		return updateFlags;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Entity> getLocalEntities() {
		return localEntities;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Location getNextLocation() {
		return nextLocation;
	}

	public void setNextLocation(Location nextLocation) {
		this.nextLocation = nextLocation;
	}

	public Location getPreviousLocation() {
		return previousLocation;
	}

	public void setPreviousLocation(Location previousLocation) {
		this.previousLocation = previousLocation;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public int getHitDiff() {
		return hitDiff;
	}

	public void setHitDiff(int hitDiff) {
		this.hitDiff = hitDiff;
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public int getCurrentHitpoints() {
		return currentHitpoints;
	}

	public void setCurrentHitpoints(int currentHitpoints) {
		this.currentHitpoints = currentHitpoints;
	}

	public int getMaxHitpoints() {
		return maxHitpoints;
	}

	public void setMaxHitpoints(int maxHitpoints) {
		this.maxHitpoints = maxHitpoints;
	}

	public int getPrimaryDirection() {
		return primaryDirection;
	}

	public void setPrimaryDirection(int primaryDirection) {
		this.primaryDirection = primaryDirection;
	}

	public int getSecondaryDirection() {
		return secondaryDirection;
	}

	public void setSecondaryDirection(int secondaryDirection) {
		this.secondaryDirection = secondaryDirection;
	}

}


