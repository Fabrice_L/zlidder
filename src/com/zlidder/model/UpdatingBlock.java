package com.zlidder.model;

import com.zlidder.util.Stream;

public abstract class UpdatingBlock {

	public abstract void update(Entity entity, Stream str);
	
}