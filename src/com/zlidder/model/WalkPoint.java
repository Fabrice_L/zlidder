package com.zlidder.model;

public final class WalkPoint {

	private int x;
	private int y;

	public WalkPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public final int getX() {
		return x;
	}

	public final int getY() {
		return y;
	}
}