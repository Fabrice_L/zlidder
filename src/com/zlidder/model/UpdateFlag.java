package com.zlidder.model;

public enum UpdateFlag {
	//All entities
	UPDATE,
	CHAT,
	APPEARANCE,
	HIT,
	FACE,
	ANIMATION,
	GRAPHIC,
	
	//Players
	TELEPORT,
	MAPREGION;	
}
