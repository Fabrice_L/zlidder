package com.zlidder.model;

public class Location {

	private int x;
	private int y;
	private int height;
		
	public Location(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
	
	public Location(int x, int y, int height) {
		this.setX(x);
		this.setY(y);
		this.setHeight(height);
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public boolean withinDistance(Location location) {
		if (getHeight() != location.getHeight()) {
			return false;
		}
		int deltaX = Math.abs(location.getX() - getX());
		int deltaY = Math.abs(location.getY() - getY());
		return deltaX < 15 && deltaY < 15;
	}
		
	public int distanceToPoint(Location location) {
		return (int) Math.sqrt(Math.pow(getX() - location.getX(), 2)
				+ Math.pow(getY() - location.getY(), 2));
	}	
	
	public void distance(Location location) {
		int deltaX = Math.abs(location.getX() - getX());
		int deltaY = Math.abs(location.getY() - getY());
		System.out.println("X: " + deltaX + ", Y: " + deltaY);
	}
	
	public boolean sameAs(Location location) {
		if (getX() == location.getX()
				&& getY() == location.getY()
				&& getHeight() == location.getHeight()) {
			return true;
		}
		return false;
	}
	
}
