package com.zlidder.model.pulse;

import com.zlidder.model.Entity;
import com.zlidder.world.NpcHandler;
import com.zlidder.world.PlayerHandler;

public class PulseOwner {

	private int id;
	private PulseType ownerType;
	
	public PulseOwner(int id, PulseType ownerType) {
		this.id = id;
		this.ownerType = ownerType;
	}
	
	public Entity getOwner() {
		switch(ownerType) {
		case NPC:
			return NpcHandler.npcs[id];
		case PLAYER:
			return PlayerHandler.players[id];
		
		}
		return null;
	}
	
	public enum PulseType {
		PLAYER,
		NPC
	}
}