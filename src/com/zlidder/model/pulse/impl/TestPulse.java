package com.zlidder.model.pulse.impl;

import com.zlidder.model.npc.Npc;
import com.zlidder.model.pulse.Pulse;
import com.zlidder.model.pulse.PulseOwner;

public class TestPulse extends Pulse {

	int pulses = 10;
	public TestPulse() {
		super(3, "test", true);
	}

	@Override
	public void execute() {
		//Npc npc = (Npc)getPulseOwner().getOwner();
		pulses --;
		if (pulses <= 0) {
			System.out.println("Last pulse sent");
			this.stop();
			return;
		}
		getPulseOwner().getOwner().playAnimation(855, 0);
	}

}
