package com.zlidder.model.pulse;

import com.zlidder.world.World;

public abstract class Pulse {

	private PulseOwner owner;
	private String pulseName;
	private int remainingPulses;
	private int pulseDelay;
	private boolean immediate;
	private boolean running = true;
	
	public Pulse(int pulses, String pulseName) {
		this.setRemainingPulses(pulses);
		this.setPulseDelay(pulses);
		this.setPulseName(pulseName);
	}
	
	public Pulse(int pulses, String pulseName, boolean immediate) {
		this(pulses, pulseName);
		this.immediate = true;
	}
	
	public abstract void execute();
	
	public void cycle() {
		if(remainingPulses-- <= 1 || immediate) {
			immediate = false;
			remainingPulses = pulseDelay;
			if(isRunning()) {
				execute();
			}
		}
	}
	
	public void pulseExists() {
		for (Pulse pulse : World.getPulseManager().getPulses()) {
			if (pulse.getPulseOwner().getOwner() == this.getPulseOwner().getOwner()
					&& pulse.getPulseName().equals(this.getPulseName())) {
				pulse.stop();
			}
		}
	}
	
	public int getRemainingPulses() {
		return remainingPulses;
	}
	
	public void setRemainingPulses(int remainingPulses) {
		this.remainingPulses = remainingPulses;
	}

	public int getPulseDelay() {
		return pulseDelay;
	}

	public void setPulseDelay(int pulseDelay) {
		this.pulseDelay = pulseDelay;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void stop() {
		running = false;
	}

	public PulseOwner getPulseOwner() {
		return owner;
	}

	public void setPulseOwner(PulseOwner owner) {
		this.owner = owner;
	}

	public String getPulseName() {
		return pulseName;
	}

	public void setPulseName(String pulseName) {
		this.pulseName = pulseName;
	}
	
}
