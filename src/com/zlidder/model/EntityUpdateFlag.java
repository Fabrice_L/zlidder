package com.zlidder.model;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Fabrice L
 *
 */

public class EntityUpdateFlag {
	
	private List<UpdateFlag> entityUpdateFlags = new LinkedList<UpdateFlag>();
	
	public void add(UpdateFlag updateFlag) {
		entityUpdateFlags.add(updateFlag);
	}
	
	public void remove(UpdateFlag updateFlag) {
		entityUpdateFlags.remove(updateFlag);
	}
	
	public boolean contains(UpdateFlag updateFlag) {
		if (entityUpdateFlags.contains(updateFlag)) {
			return true;
		}
		return false;
	}

}

