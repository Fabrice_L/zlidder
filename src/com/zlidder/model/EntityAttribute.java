package com.zlidder.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Fabrice L
 *
 */

public class EntityAttribute {
		
	private Map<AttributeType, Object> entityAttributes = new HashMap<AttributeType, Object>();
		
	public void add(AttributeType playerAttribute, Object object) {
		entityAttributes.put(playerAttribute, object);
	}
	
	public void remove(AttributeType playerAttribute) {
		entityAttributes.remove(playerAttribute);
	}
	
	public boolean contains(AttributeType playerAttribute) {
		if (entityAttributes.containsKey(playerAttribute)) {
			return true;
		}
		return false;
	}
	
	public String getString(AttributeType playerAttribute) {
		if (entityAttributes.containsKey(playerAttribute)) {
			return entityAttributes.get(playerAttribute).toString();	
		}
		return null;
	}
	
	public int getInteger(AttributeType playerAttribute) {
		if (entityAttributes.containsKey(playerAttribute)) {
			return (int) entityAttributes.get(playerAttribute);
		}
		return -1;
	}
	
	public double getDouble(AttributeType playerAttribute) {
		if (entityAttributes.containsKey(playerAttribute)) {
			return (double) entityAttributes.get(playerAttribute);
		}
		return -1;
	}
	
	public boolean getBoolean(AttributeType playerAttribute) {
		if (entityAttributes.containsKey(playerAttribute)) {
			return (boolean) entityAttributes.get(playerAttribute);
		}
		return false;
	}
}

