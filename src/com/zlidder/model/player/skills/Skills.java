package com.zlidder.model.player.skills;

import com.zlidder.model.UpdateFlag;
import com.zlidder.model.player.Player;
import com.zlidder.net.packet.outgoing.SendMessage;
import com.zlidder.net.packet.outgoing.ShowSkill;


public class Skills {
	
	public Player player;
	
	public int[] playerLevel = new int[25];
	public int[] playerXP = new int[25];

	public Skills(Player player) {
		this.player = player;
	}

	/**
	 * The skill names. Skill 18 and 19 or not in the player. (farming and
	 * slayer).
	 */
	public static final String[][] SKILL_NAME = { { "an attack", "Attack" },
			{ "a defence", "Defence" }, { "a strength", "Strength" },
			{ "a hitpoints", "Hitpoints" }, { "a ranged", "Ranging" },
			{ "a prayer", "Prayer" }, { "a magic", "Magic" },
			{ "a cooking", "Cooking" }, { "a woodcutting", "Woodcutting" },
			{ "a fletching", "Fletching" }, { "a fishing", "Fishing" },
			{ "a firemaking", "Fishing" }, { "a crafting", "Crafting" },
			{ "a smithing", "Smithing" }, { "a mining", "Aining" },
			{ "a herblore", "Herblore" }, { "an agility", "Agility" },
			{ "a thieving", "Thieving" }, { "a slayer", "Slayer" },
			{ "a farming", "Farming" }, { "a runecraft", "Runecraft" } };

	/**
	 * Adds experience.
	 * 
	 * @param amount
	 *            The experience.
	 * @param skill
	 *            The skill.
	 * @return
	 */
	public boolean addExp(double amount, int skill) {
		int oldLevel = getLevelForXP(skill);
		int currentHp = playerLevel[3];
		if (amount + playerXP[skill] < 0
				|| playerXP[skill] > 200000000) {
			player.send(new SendMessage("EXP Limit Reached!"));
			return false;
		}
		playerXP[skill] += amount;
		if (skill != 3) {
			playerLevel[skill] = getLevelForXP(skill);
		} else if (skill == 3) {
			playerLevel[3] = currentHp;
		}
		if (oldLevel < getLevelForXP(skill)) {
			player.getActionSender().sendLevelUp(skill);
		}
		player.getUpdateFlags().add(UpdateFlag.UPDATE);
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		player.send(new ShowSkill(skill));
		return true;
	}

	public int getXPForLevel(int level) {
		int points = 0;
		int output = 0;

		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor((double) lvl + 300.0
					* Math.pow(2.0, (double) lvl / 7.0));
			if (lvl >= level)
				return output;
			output = (int) Math.floor(points / 4);
		}
		return 0;
	}

	   public int getLevelForXP(int skillId) {
	        int exp = playerXP[skillId];
	        int points = 0;
	        int output = 0;
	        for (int lvl = 1; lvl < 100; lvl++) {
	            points += Math.floor((double)lvl + 300.0 * Math.pow(2.0, (double)lvl / 7.0));
	            output = (int)Math.floor(points / 4);
	            if ((output - 1) >= exp) {
	                return lvl;
	            }
	        }
	        return 99;
	    }
	   
}