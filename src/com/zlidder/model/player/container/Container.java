package com.zlidder.model.player.container;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.net.packet.outgoing.SendMessage;

public abstract class Container {

	private Player player;
	
	private int capacity;
	
	private GameItem[] items;
	
	public Container(Player player, int capacity) {
		this.player = player;
		this.capacity = capacity;
		this.items = new GameItem[capacity];
		
		for (int i = 0; i < capacity; i++) {
			getItems()[i] = new GameItem(0, 0);
		}
	}
	
	public GameItem[] getItems() {
		return items;
	}
	
	public void setItems(GameItem[] items) {
		this.items = items;
	}
	
	public abstract boolean addItem(GameItem item, int slot);
	
	public abstract boolean removeItem(GameItem item, int slot);
	
	public boolean addItem(GameItem item) {
		return addItem(item, -1);
	}
	
	public boolean removeItem(GameItem item) {
		return removeItem(item, -1);
	}
	
	public abstract void switchItems(int slot, int otherSlot);
		
	public int getItemSlot(GameItem item) {
		for (int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() == item.getId()) {
				return slot;
			}
		}
		return -1;	
	}
	
	public boolean containsItem(GameItem item) {
		for (GameItem items : getItems()) {
			if (items.getId() == item.getId()) {
				return true;
			}
		}
		return false;
	}

	public boolean containsItemAmount(GameItem item) {
		for (GameItem items : getItems()) {
			if (items.getId() == item.getId() && items.getAmount() == item.getAmount()) {
				return true;
			}
		}
		getPlayer().send(new SendMessage("You don't have enough " + item.getName() + " stored to do this"));
		return false;
	}
	
	public boolean hasFreeSlots() {
		for(GameItem item : getItems()) {
			if (item.getId() < 1) {
				return true;
			}
		}
		return false;
	}
	
	public int getFreeSlot() {
		for(int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() < 1) {
				return slot;
			}
		}
		return -1;
	}
	
	public int freeSlotAmount() {
		int freeSlots = 0;
		for (int slot = 0; slot < capacity; slot++) {
			if (getItems()[slot].getId() < 1) {
				freeSlots++;
			}
		}
		return freeSlots;
	}
	
	public int getItemAmount(GameItem item, boolean stacked) {
		int amount = 0;
		for(GameItem items : getItems()) {
			if(items.getId() == item.getId()) {
				if (stacked) {
					return items.getAmount();
				}
				amount++;
			}
		}
		return amount;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public enum ContainerType {
		BANK(304, 5382),
		INVENTORY(28, 3214),
		INVENTORY_BANK(28, 5064),
		EQUIPMENT(14, 1688);
		
		private int capacity;
		private int moveWindow;
		
		ContainerType(int capacity, int moveWindow) {
			this.capacity = capacity;
			this.moveWindow = moveWindow;
		}
		
		public int getCapacity() {
			return capacity;
		}
		
		public int getMoveWindow() {
			return moveWindow;
		}
	}
}
