package com.zlidder.model.player.container.impl;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.model.player.container.Container;
import com.zlidder.net.packet.outgoing.RefreshItems;

public class EquipmentContainer extends Container {

	public EquipmentContainer(Player player) {
		super(player, ContainerType.EQUIPMENT.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item, int slot) {
		if (containsItem(item) && item.isStackable()) {
			getItems()[slot].addAmount(item.getAmount());
		} else {
			getItems()[slot] = item;
		}
		getPlayer().send(new RefreshItems(this, ContainerType.EQUIPMENT));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item, int slot) {
		// TODO Auto-generated method stub
		if (!containsItem(item)) {
			return false;
		}
		getItems()[slot] = new GameItem(0, 0);
		getPlayer().send(new RefreshItems(this, ContainerType.EQUIPMENT));
		return true;
	}

	@Override
	public void switchItems(int slot, int otherSlot) {
		// TODO Auto-generated method stub

	}

}
