package com.zlidder.model.player.container.impl;

import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.model.player.container.Container;
import com.zlidder.net.packet.outgoing.RefreshItems;
import com.zlidder.net.packet.outgoing.SendMessage;

public class InventoryContainer extends Container{

	public InventoryContainer(Player player) {
		super(player, ContainerType.INVENTORY.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item, int slot) {
		if(item.getAmount() < 1) {
			return false;
		}
		
		if((!hasFreeSlots() || getFreeSlot() < 0) && !(containsItem(item) && item.isStackable())) {
			getPlayer().send(new SendMessage("There isn't enough storage space to do this!"));
			return false;
		}
		
		int itemSlot = slot > 0 ? slot : getItemSlot(item);
		System.out.println("using slot: " + itemSlot);
		
		if(containsItem(item) && item.isStackable() && itemSlot >= 0) {
			if (getItems()[itemSlot].addAmount(item.getAmount()) < 1) {
				getItems()[itemSlot].setAmount(Integer.MAX_VALUE);
			}
		} else {
			if (item.getAmount() == 1) {
					getItems()[getFreeSlot()] = item;
			} else {
				int amount = item.getAmount();
				if(!item.isStackable()) {
					while (amount > 0) {
						if (hasFreeSlots()) {
							getItems()[getFreeSlot()] = new GameItem(item.getId(), 1);
							amount--;
						} else {
							amount = 0;
							getPlayer().send(new SendMessage("There isn't enough storage space to do this!"));
						}
					}
				} else {
					if(hasFreeSlots()) {
						getItems()[getFreeSlot()] = item;
					} else {
						getPlayer().send(new SendMessage("There isn't enough storage space to do this!"));
					}
				}
			}
		}
		getPlayer().send(new RefreshItems(this, ContainerType.INVENTORY));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item, int slot) {
		if (!containsItem(item)) {
			return false;
		}
		
		if (item.getAmount() < 1) {
			return false;
		}
		
		int itemSlot = slot > 0 ? slot : getItemSlot(item);
		if (item.isStackable()) {
			if (getItems()[itemSlot].removeAmount(item.getAmount()) < 1) {
				getItems()[itemSlot] = new GameItem(0, 0);
			}
		} else {
			if (item.getAmount() == 1) {
				getItems()[itemSlot] = new GameItem(0, 0);
			} else {
				int amount = item.getAmount();
				while (containsItem(item) && amount > 0) {
					getItems()[getItemSlot(item)] = new GameItem(0, 0);
					amount--;
				}
			}
		}
		getPlayer().send(new RefreshItems(this, ContainerType.INVENTORY));
		return true;
	}

	@Override
	public void switchItems(int slot, int otherSlot) {
		GameItem item = getItems()[slot];
		GameItem otherItem = getItems()[otherSlot];
		if(slot >= 0 && otherSlot >= 0 && slot <= getCapacity() && otherSlot <= getCapacity()) {
			getItems()[slot] = otherItem;
			getItems()[otherSlot] = item;
			getPlayer().send(new RefreshItems(this, ContainerType.INVENTORY));
			getPlayer().send(new RefreshItems(this, ContainerType.INVENTORY_BANK));
		}	
	}

}
