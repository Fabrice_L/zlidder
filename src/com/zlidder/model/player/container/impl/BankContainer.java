package com.zlidder.model.player.container.impl;

import com.zlidder.io.cache.def.ItemDefinition;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.player.Player;
import com.zlidder.model.player.container.Container;
import com.zlidder.net.packet.outgoing.InventoryOverlay;
import com.zlidder.net.packet.outgoing.RefreshItems;
import com.zlidder.net.packet.outgoing.SendMessage;

public class BankContainer extends Container {

	public BankContainer(Player player) {
		super(player, ContainerType.BANK.getCapacity());
		for(int i = 0; i < 10; i++) {
			getItems()[i] = new GameItem(1321 + (i*2), 100);
		}
	}
	
	private boolean notesEnabled;
	private boolean insertItems;
	
	public void open() {
		getPlayer().send(new InventoryOverlay(5292, 5063));
		getPlayer().send(new RefreshItems(getPlayer().getInventoryContainer(), ContainerType.INVENTORY_BANK));
	}

	@Override
	public boolean addItem(GameItem item, int slot) {
		if(item.getAmount() < 1) {
			return false;
		}
		
		if((!hasFreeSlots() || getFreeSlot() < 0) && !containsItem(item)) {
			getPlayer().send(new SendMessage("There isn't enough storage space to do this!"));
			return false;
		}
		
		if (item.getAmount() > getPlayer().getInventoryContainer().getItemAmount(item, item.isStackable())) {
			item.setAmount(getPlayer().getInventoryContainer().getItemAmount(item, item.isStackable()));
		}
		
		GameItem newItem = new GameItem(item.noteToItem(), item.getAmount());
		
				
		if(containsItem(newItem) && getItemSlot(newItem) >= 0) {
			if (getItems()[getItemSlot(newItem)].addAmount(newItem.getAmount()) < 1) {
				getItems()[getItemSlot(newItem)].setAmount(Integer.MAX_VALUE);
			}
			getPlayer().getInventoryContainer().removeItem(item, slot);
		} else {
			getItems()[getFreeSlot()] = newItem;
			getPlayer().getInventoryContainer().removeItem(item, slot);
		}

		getPlayer().send(new RefreshItems(this, ContainerType.BANK));
		getPlayer().send(new RefreshItems(getPlayer().getInventoryContainer(), ContainerType.INVENTORY_BANK));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item, int slot) {
		if (!containsItem(item)) {
			return false;
		}
		
		if (item.getAmount() < 1) {
			return false;
		}
		
		int amount = item.getAmount();
		int itemId = isNotesEnabled()? item.itemToNote() : item.getId();
				
		if (getItemAmount(item, true) < amount) {
			amount = getItemAmount(item, true);
		}
		
		if (item.isStackable() && isNotesEnabled()) {
			getPlayer().send(new SendMessage("You can't withdraw this item as a note"));
		}
						
		if (amount > getPlayer().getInventoryContainer().freeSlotAmount() && !ItemDefinition.lookup(itemId).isStackable()) {
			amount = getPlayer().getInventoryContainer().freeSlotAmount();
		}
				
		getPlayer().getInventoryContainer().addItem(new GameItem(itemId, amount));
				
		if (getItems()[slot].removeAmount(amount) < 1) {
			getItems()[slot] = new GameItem(0, 0);
		}
		
		getPlayer().send(new RefreshItems(this, ContainerType.BANK));
		getPlayer().send(new RefreshItems(getPlayer().getInventoryContainer(), ContainerType.INVENTORY_BANK));
		return true;
	}

	@Override
	public void switchItems(int slot, int otherSlot) {
		GameItem item = getItems()[slot];
		GameItem otherItem = getItems()[otherSlot];
		if(slot >= 0 && otherSlot >= 0 && slot <= getCapacity() && otherSlot <= getCapacity()) {
			if (isInsertItems()) {
				GameItem from = getItems()[slot];
				if(from == null) {
					return;
				}
				getItems()[slot] = null;
				if(slot > otherSlot) {
					int shiftFrom = otherSlot;
					int shiftTo = slot;
					for(int i = (otherSlot + 1); i < slot; i++) {
						if(getItems()[i] == null) {
							shiftTo = i;
							break;
						}
					}
					GameItem[] slice = new GameItem[shiftTo - shiftFrom];
					System.arraycopy(getItems(), shiftFrom, slice, 0, slice.length);
					System.arraycopy(slice, 0, getItems(), shiftFrom + 1, slice.length);
				} else {
					int sliceStart = slot + 1;
					int sliceEnd = otherSlot;
					for(int i = (sliceEnd - 1); i >= sliceStart; i--) {
						if(getItems()[i] == null) {
							sliceStart = i;
							break;
						}
					}
					GameItem[] slice = new GameItem[sliceEnd - sliceStart + 1];
					System.arraycopy(getItems(), sliceStart, slice, 0, slice.length);
					System.arraycopy(slice, 0, getItems(), sliceStart - 1, slice.length);
				}
				getItems()[otherSlot] = from;
			} else {
				getItems()[slot] = otherItem;
				getItems()[otherSlot] = item;
			}
			getPlayer().send(new RefreshItems(this, ContainerType.BANK));
		}	
	}

	public boolean isNotesEnabled() {
		return notesEnabled;
	}

	public void setNotesEnabled(boolean notesEnabled) {
		this.notesEnabled = notesEnabled;
	}

	public boolean isInsertItems() {
		return insertItems;
	}

	public void setInsertItems(boolean insertItems) {
		this.insertItems = insertItems;
	}

}
