package com.zlidder.model.player;

public class PlayerAppearance {

	private int[] playerAppearance = new int[13];
	
	public void set(Appearance appearance, int index) {
		playerAppearance[appearance.getAppearanceId()] = index;
	}
	
	public int get(Appearance appearance) {
		return playerAppearance[appearance.getAppearanceId()];
	}
	
	public void setDefault() {
		for (int i = 0; i < playerAppearance.length; i++) {
			Appearance appearance = Appearance.getAppearanceById(i);
			playerAppearance[i] = appearance.getDefaultId();
		}
	}
	
	public enum Appearance {
		GENDER(0, 0),
		HEAD(1, 0),
		TORSO(2, 18),
		ARMS(3, 26),
		HANDS(4, 33),
		LEGS(5, 36),
		FEET(6, 42),
		BEARD(7, 10),
		
		HAIR_COLOUR(8, 7),
		TORSO_COLOUR(9, 8),
		LEGS_COLOUR(10, 9),
		FEET_COLOUR(11, 5),
		SKIN_COLOUR(12, 0);	
		
		private int appearanceId;
		private int defaultId;
		
		Appearance(int appearanceId, int defaultId) {
			this.appearanceId = appearanceId;
			this.defaultId = defaultId;
		}
		
		public int getAppearanceId() {
			return appearanceId;
		}
		
		public int getDefaultId() {
			return defaultId;
		}
		
		public static Appearance getAppearanceById(int id) {
			for (Appearance appearance : Appearance.values()) {
				if (appearance.getAppearanceId() == id){
					return appearance;
				}
			}
			return null;
		}
	}
	
}
