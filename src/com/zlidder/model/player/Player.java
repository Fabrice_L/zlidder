package com.zlidder.model.player;

import java.util.ArrayList;
import java.util.HashMap;

import com.zlidder.Config;
import com.zlidder.io.ActionSender;
import com.zlidder.model.Entity;
import com.zlidder.model.Location;
import com.zlidder.model.Region;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.item.Equipment;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.PlayerAppearance.Appearance;
import com.zlidder.model.player.container.Container.ContainerType;
import com.zlidder.model.player.container.impl.BankContainer;
import com.zlidder.model.player.container.impl.EquipmentContainer;
import com.zlidder.model.player.container.impl.InventoryContainer;
import com.zlidder.model.player.skills.Skills;
import com.zlidder.net.PacketManager;
import com.zlidder.net.packet.IncomingPacket;
import com.zlidder.net.packet.OutgoingPacket;
import com.zlidder.net.packet.outgoing.CloseInterface;
import com.zlidder.net.packet.outgoing.Logout;
import com.zlidder.net.packet.outgoing.SendMessage;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;
import com.zlidder.world.ItemHandler;
import com.zlidder.world.NpcHandler;
import com.zlidder.world.PlayerHandler;
import com.zlidder.world.RegionHandler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;


public class Player extends Entity{

	private Channel channel;
	
	private InventoryContainer inventoryContainer = new InventoryContainer(this);
	private BankContainer bankContainer = new BankContainer(this);
	private EquipmentContainer equipmentContainer = new EquipmentContainer(this);
	
    public ArrayList<Player> playersUpdating = new ArrayList<Player>();
	
	private Skills skills = new Skills(this);
	private PacketManager packetManager = new PacketManager(this);
	private ActionSender actionSender = new ActionSender(this);
	private Equipment equipment = new Equipment(this);
	private PlayerAppearance appearance = new PlayerAppearance();
	
	public long lastCycle = System.currentTimeMillis();
	private String playerName;
	private String password;
	
	public Skills getSkills() {
		return skills;
	}
	
	public Equipment getEquipment() {
		return equipment;
	}
	
	public PacketManager packetManager() {
		return packetManager;
	}
	
	public InventoryContainer getInventoryContainer() {
		return inventoryContainer;
	}
	
	public BankContainer getBankContainer() {
		return bankContainer;
	}
	
	public EquipmentContainer getEquipmentContainer() {
		return equipmentContainer;
	}
	
	public PlayerAppearance getAppearance() {
		return appearance;
	}

	public void setAppearance(PlayerAppearance appearance) {
		this.appearance = appearance;
	}

	public ActionSender getActionSender() {
		return actionSender;
	}
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public final void send(Object object) {
		if (object instanceof OutgoingPacket) {
			((OutgoingPacket) object).send(this);
		}
		if (object instanceof IncomingPacket) {
			((IncomingPacket) object).send(this);
		}
		if (object instanceof Stream) {
				byte[] byteArray = ((Stream)object).buffer;
				ByteBuf buf = Unpooled.wrappedBuffer(byteArray);
				buf.capacity(((Stream)object).currentOffset);
				if (!this.disconnected)
				getChannel().write(buf);
		}
		if (object instanceof ByteBuf) {
			if (object != null && this != null) {
				getChannel().write((ByteBuf)object);
			}
		}
	}

	public void destruct() {
		playerListSize = 0;
		for (int i = 0; i < maxPlayerListSize; i++)
			playerList[i] = null;

		for (int i = 0; i < maxNPCListSize; i++)
			npcList[i] = null;
				
		setLocation(new Location(-1, -1));
		mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		resetWalkingQueue();
		if (getChannel() == null)
			return; // already shutdown
		Misc.println("ClientHandler: Client " + playerName + " disconnected.");
			disconnected = true;
			getChannel().close();
			setChannel(null);
			isActive = false;
			playerName = null;
			synchronized (this) {
				notify();
			} // make sure this threads gets control so it can terminate
		PlayerHandler.clientCount -= 1;
	}

	public int FocusPointX = -1, FocusPointY = -1;
	
	public int NewHP = 100;
	public int hptype = 0;
	public boolean poisondmg = false;

	public boolean updateFaceCoords = false;
    public int faceX = -1;
    public int faceY = -1;

    public void faceCoords(int x, int y) {
/*
        faceX = 2 * x + 1;
	faceY = 2 * y + 1;
	updateFaceCoords = true;
        updateRequired = true;
*/
		Misc.println(x+" ,"+y);
    }

	public int hitOnPlayer = 0;

	public double playerEnergy = 100;
	public boolean RebuildNPCList;

	public int pEmote = 0x328;
	public int pWalk = 0x333;
	public int playerSE = 0x328; // SE = Standard Emotion
	public int playerSEW = 0x333; // SEW = Standard Emotion Walking
	public int playerSER = 0x338; // SER = Standard Emotion Run
	public int attackEmote = 0x326; // SEA = Standard Emotion Attack
	public int blockEmote = 0x326; // SEA = Standard Emotion Block

	public Player(Channel channel, int slot) {
		this.setChannel(channel);
		this.setEntityId(slot);
		this.playerRights = 2;
	}
	
	public void login() {
		System.out.println("trying to log in");
		int response = 2;

		// Check if the player is already logged in.
		for (Player player : PlayerHandler.players) {
			if (player == null) {
				continue;
			}
			if (PlayerHandler.isPlayerOn(playerName)) {
				response = 5;
			}
		}
		isActive = true;
		PlayerHandler.addClient(getEntityId(), this);
		ByteBuf resp = Unpooled.buffer(3);
		resp.writeByte(response);
		resp.writeByte(playerRights);
		resp.writeByte(0);
		send(resp);
		if (response != 2) {
			destruct();
			return;
		}
		
		getSkills();
		this.getInventoryContainer().addItem(new GameItem(1333, 3));
		this.getInventoryContainer().addItem(new GameItem(1321, 1));
		for (int i = 0; i < getSkills().playerXP.length; i++) // Setting XP for Levels.
													// Player levels are shown
													// as "Level/LevelForXP(XP)"
		{
			if (i == 3) {
				getSkills().playerLevel[i] = 10;
				getSkills().playerXP[i] = 1155;
			} else {
				getSkills().playerLevel[i] = 1;
				getSkills().playerXP[i] = 0;
			}
			//getActionSender().refreshSkill(i);
		}
		
		getAppearance().setDefault();
		

		// initial x and y coordinates of the player
		// the first call to updateThisPlayerMovement() will craft the proper
		// initialization packet
		// lumbridge coords
		setNextLocation(new Location(Config.STARTPOS_X + Misc.random(2), Config.STARTPOS_Y + Misc.random(2), 0));
		setLocation(getNextLocation());
		
		
		for (Region region : RegionHandler.getRegions()) {
			if (region.getRegionX() == getLocation().getX()/64 
					&& region.getRegionY() == getLocation().getY()/64) {
				region.addEntity(this);
				setRegion(region);
			}
		}
		

		// Client initially doesn't know those values yet
		mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		resetWalkingQueue();
		//World.submit(new EntityUpdatePulse(new PulseOwner(this.getEntityId(), PulseType.PLAYER)));
	
	}

	public boolean initialized = false, disconnected = false;
	public boolean isActive = false;
	
	public int playerRights; // 0=normal player, 1=player mod, 2=real mod,
								// 3=admin?


	public int playerHat = 0;
	public int playerCape = 1;
	public int playerAmulet = 2;
	public int playerWeapon = 3;
	public int playerChest = 4;
	public int playerShield = 5;
	public int playerLegs = 7;
	public int playerHands = 9;
	public int playerFeet = 10;
	public int playerRing = 12;
	public int playerArrows = 13;

	// the list of players currently seen by thisPlayer
	// this has to be remembered because the Client will build up exactly the
	// same list
	// and will be used on subsequent player movement update packets for
	// efficiency
	public final static int maxPlayerListSize = PlayerHandler.maxPlayers;
	public Player playerList[] = new Player[maxPlayerListSize];
	public int playerListSize = 0;
	public int mapRegionX, mapRegionY; // the map region the player is currently
										// in
	//public int absX, absY; // absolute x/y coordinates
	public int currentX, currentY; // relative x/y coordinates (to map region)
	// Note that mapRegionX*8+currentX yields absX

	//public boolean updateRequired = true; // set to true if, in general,
											// updating for this player is
											// required
											// i.e. this should be set to true
											// whenever any of the other
											// XXXUpdateRequired flags are set
											// to true
											// Important: this does NOT include
											// chatTextUpdateRequired!

	// walking related stuff - walking queue etc...
	public static final int walkingQueueSize = 50;
	public int walkingQueueX[] = new int[walkingQueueSize],
			walkingQueueY[] = new int[walkingQueueSize];
	public int wQueueReadPtr = 0; // points to slot for reading from queue
	public int wQueueWritePtr = 0; // points to (first free) slot for writing to
									// the queue
	public boolean isRunning = false;

	public void TurnPlayerTo(int pointX, int pointY) {
		FocusPointX = 2 * pointX + 1;
		FocusPointY = 2 * pointY + 1;
	}

	public void resetWalkingQueue() {
		wQueueReadPtr = wQueueWritePtr = 0;
		// properly initialize this to make the "travel back" algorithm work
		for (int i = 0; i < walkingQueueSize; i++) {
			walkingQueueX[i] = currentX;
			walkingQueueY[i] = currentY;
		}
	}

	public void addToWalkingQueue(int x, int y) {
		int next = (wQueueWritePtr + 1) % walkingQueueSize;
		if (next == wQueueWritePtr)
			return; // walking queue full, silently discard the data
		walkingQueueX[wQueueWritePtr] = x;
		walkingQueueY[wQueueWritePtr] = y;
		wQueueWritePtr = next;
	}

	// returns 0-7 for next walking direction or -1, if we're not moving
	public int getNextWalkingDirection() {
		if (wQueueReadPtr == wQueueWritePtr)
			return -1; // walking queue empty
		int dir;
		do {
			dir = Misc.direction(currentX, currentY,
					walkingQueueX[wQueueReadPtr], walkingQueueY[wQueueReadPtr]);
			if (dir == -1)
				wQueueReadPtr = (wQueueReadPtr + 1) % walkingQueueSize;
			else if ((dir & 1) != 0) {
				resetWalkingQueue();
				return -1;
			}
		} while (dir == -1 && wQueueReadPtr != wQueueWritePtr);
		if (dir == -1)
			return -1;
		dir >>= 1;
		currentX += Misc.directionDeltaX[dir];
		currentY += Misc.directionDeltaY[dir];
		setLocation(new Location(getLocation().getX() + Misc.directionDeltaX[dir], getLocation().getY() + Misc.directionDeltaY[dir], getLocation().getHeight()));
		return dir;
	}
	
	public boolean createItems = false;

	public void getNextPlayerMovement() {
		getUpdateFlags().remove(UpdateFlag.MAPREGION);
		getUpdateFlags().remove(UpdateFlag.TELEPORT);
		setPrimaryDirection(-1);
		setSecondaryDirection(-1);
		setPreviousLocation(getLocation());
		if (getNextLocation() != null) {
			getUpdateFlags().add(UpdateFlag.MAPREGION);
			if (mapRegionX != -1 && mapRegionY != -1) {
				// check, whether destination is within current map region
				int relX = getNextLocation().getX() - mapRegionX * 8, relY = getNextLocation().getY()
						- mapRegionY * 8;
				if (relX >= 2 * 8 && relX < 11 * 8 && relY >= 2 * 8
						&& relY < 11 * 8)
					getUpdateFlags().remove(UpdateFlag.MAPREGION);
			}
			if (getUpdateFlags().contains(UpdateFlag.MAPREGION)) {
				// after map region change the relative coordinates range
				// between 48 - 55
				mapRegionX = (getNextLocation().getX() >> 3) - 6;
				mapRegionY = (getNextLocation().getY() >> 3) - 6;

				playerListSize = 0; // completely rebuild playerList after
									// teleport AND map region change
			}

			currentX = getNextLocation().getX() - 8 * mapRegionX;
			currentY = getNextLocation().getY() - 8 * mapRegionY;
			//setLocation(nextLocation);
			setLocation(getNextLocation());
			resetWalkingQueue();

			setNextLocation(null);
			getUpdateFlags().add(UpdateFlag.TELEPORT);
		} else {
			setPrimaryDirection(getNextWalkingDirection());
			if (getPrimaryDirection() == -1)
				return; // standing

			boolean canRun = playerEnergy > 0;
			if (isRunning) {
				if (canRun)
					setSecondaryDirection(getNextWalkingDirection());
			}

			// check, if we're required to change the map region
			int deltaX = 0, deltaY = 0;
			if (currentX < 2 * 8) {
				deltaX = 4 * 8;
				mapRegionX -= 4;
				getUpdateFlags().add(UpdateFlag.MAPREGION);
			} else if (currentX >= 11 * 8) {
				deltaX = -4 * 8;
				mapRegionX += 4;
				getUpdateFlags().add(UpdateFlag.MAPREGION);
			}
			if (currentY < 2 * 8) {
				deltaY = 4 * 8;
				mapRegionY -= 4;
				getUpdateFlags().add(UpdateFlag.MAPREGION);
			} else if (currentY >= 11 * 8) {
				deltaY = -4 * 8;
				mapRegionY += 4;
				getUpdateFlags().add(UpdateFlag.MAPREGION);
			}

			if (getUpdateFlags().contains(UpdateFlag.MAPREGION)) {
				// have to adjust all relative coordinates
				currentX += deltaX;
				currentY += deltaY;
				for (int i = 0; i < walkingQueueSize; i++) {
					walkingQueueX[i] += deltaX;
					walkingQueueY[i] += deltaY;
				}
			}

		}
	}
	public byte cachedPropertiesBitmap[] = new byte[(PlayerHandler.maxPlayers + 7) >> 3];
	
	
	public void appendPlayerAppearance(Stream str) {
		Stream playerProps = new Stream(new byte[100]);

		playerProps.currentOffset = 0;
		playerProps.writeByte(getAppearance().get(Appearance.GENDER));
		playerProps.writeByte(0);
		//playerProps.writeByte(-1);
		if (getEquipmentContainer().getItems()[playerHat].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerHat].getId());
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerCape].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerCape].getId());
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerAmulet].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerAmulet].getId());
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerWeapon].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerWeapon].getId());
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerChest].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerChest].getId());
		} else {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.TORSO));
		}
		if (getEquipmentContainer().getItems()[playerShield].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerShield].getId());
		} else {
			playerProps.writeByte(0);
		}
		if (!Equipment.isPlate(getEquipmentContainer().getItems()[playerChest].getId())) {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.ARMS));
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerLegs].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerLegs].getId());
		} else {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.LEGS));
		}
		if (!Equipment.isFullHelm(getEquipmentContainer().getItems()[playerHat].getId())
				&& !Equipment.isFullMask(getEquipmentContainer().getItems()[playerHat].getId())) {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.HEAD)); // head
		} else {
			playerProps.writeByte(0);
		}
		if (getEquipmentContainer().getItems()[playerHands].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerHands].getId());
		} else {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.HANDS));
		}
		if (getEquipmentContainer().getItems()[playerFeet].getId() > 1) {
			playerProps.writeWord(0x200 + getEquipmentContainer().getItems()[playerFeet].getId());
		} else {
			playerProps.writeWord(0x100 + getAppearance().get(Appearance.FEET));
		}
		playerProps.writeByte(0);

		playerProps.writeByte(getAppearance().get(Appearance.HAIR_COLOUR)); // hair color
		playerProps.writeByte(getAppearance().get(Appearance.TORSO_COLOUR)); // torso color.
		playerProps.writeByte(getAppearance().get(Appearance.LEGS_COLOUR)); // leg color
		playerProps.writeByte(getAppearance().get(Appearance.FEET_COLOUR)); // feet color
		playerProps.writeByte(getAppearance().get(Appearance.SKIN_COLOUR)); // skin color (0-6)

			if(pEmote < 2260) {
		playerProps.writeWord(pEmote); // standAnimIndex
			} else {
		playerProps.writeWord(0x328); // standAnimIndex
			}

		playerProps.writeWord(0x337); 

			if(playerSEW < 2260) {
		playerProps.writeWord(playerSEW); // walkAnimIndex
			} else {
		playerProps.writeWord(0x333);
			}

		playerProps.writeWord(0x334); // turn180AnimIndex
		playerProps.writeWord(0x335); // turn90CWAnimIndex
		playerProps.writeWord(0x336); // turn90CCWAnimIndex
			if(playerSER < 2260) {
		playerProps.writeWord(playerSER); // runAnimIndex
			} else {
		playerProps.writeWord(0x338); 
			}

		playerProps.writeQWord(Misc.playerNameToInt64(playerName));

        int attack = getSkills().getLevelForXP(0);
        int defence = getSkills().getLevelForXP(1);
        int strength = getSkills().getLevelForXP(2);
        int hp = getSkills().getLevelForXP(3);
        int prayer = getSkills().getLevelForXP(5);
        int ranged = getSkills().getLevelForXP(4);
        int magic = getSkills().getLevelForXP(6);
        int combatLevel = (int) ((defence + hp + Math.floor(prayer / 2) + Math.ceil((1 / 2) + 1)) * 0.25);
        double melee = (attack + strength) * 0.325; 
        double ranger = Math.floor(ranged * 1.5) * 0.325; 
        double mage = Math.floor(magic * 1.5) * 0.325; 
        if (melee >= ranger && melee >= mage) {
            combatLevel += melee;
        } else if (ranger >= melee && ranger >= mage) {
            combatLevel += ranger;
        } else if (mage >= melee && mage >= ranger) {
            combatLevel += mage;
        }

		playerProps.writeByte(combatLevel); // combat level
		playerProps.writeWord(0); // incase != 0, writes skill-%d

		str.writeByteC(playerProps.currentOffset); // size of player appearance
													// block
		str.writeBytes(playerProps.buffer, playerProps.currentOffset, 0);
	}
	
	public void appendHitUpdate(Stream str) {
		try {
			str.writeByte(hitOnPlayer); // What the perseon got 'hit' for
			if (hitOnPlayer > 0 && poisondmg == false) {
				str.writeByteS(1); // 0: red hitting - 1: blue hitting
			} else if (hitOnPlayer > 0 && poisondmg == true) {
				str.writeByteS(2); // 0: red hitting - 1: blue hitting 2: poison
									// 3: orange
			} else {
				str.writeByteS(0); // 0: red hitting - 1: blue hitting
			}
			NewHP = (getSkills().playerLevel[3] - hitOnPlayer);
			if (NewHP <= 0) {
				NewHP = 0;
				setDead(true);
			}
			str.writeByte(NewHP); // Their current hp, for HP bar
			str.writeByteC(getSkills().getLevelForXP(3)); // Their
																		// max
																		// hp,
																		// for
																		// HP
																		// bar
			poisondmg = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final int maxNPCListSize = NpcHandler.maxNPCs;
	public Npc npcList[] = new Npc[maxNPCListSize];
	public int npcListSize = 0;
	public byte npcInListBitmap[] = new byte[(NpcHandler.maxNPCs+7) >> 3];
	
	public byte chatText[] = new byte[4096], chatTextSize = 0;
	public int chatTextEffects = 0, chatTextColor = 0;

	public void clearUpdateFlags() {
		getUpdateFlags().remove(UpdateFlag.ANIMATION);
		getUpdateFlags().remove(UpdateFlag.UPDATE);
		getUpdateFlags().remove(UpdateFlag.CHAT);
		getUpdateFlags().remove(UpdateFlag.APPEARANCE);
		getUpdateFlags().remove(UpdateFlag.HIT);
		getUpdateFlags().remove(UpdateFlag.FACE);
		faceEntity(null);
	}

	public static int newWalkCmdX[] = new int[walkingQueueSize];
	public static int newWalkCmdY[] = new int[walkingQueueSize];
	public static int newWalkCmdSteps = 0;
	public static boolean newWalkCmdIsRunning = false;
	public static int travelBackX[] = new int[walkingQueueSize];
	public static int travelBackY[] = new int[walkingQueueSize];
	public static int numTravelBackSteps = 0;

	public void postProcessing() {
		if (newWalkCmdSteps > 0) {
			int firstX = newWalkCmdX[0], firstY = newWalkCmdY[0];
			int lastDir = 0;
			boolean found = false;
			numTravelBackSteps = 0;
			int ptr = wQueueReadPtr;
			int dir = Misc.direction(currentX, currentY, firstX, firstY);
			if (dir != -1 && (dir & 1) != 0) {
				do {
					lastDir = dir;
					if (--ptr < 0)
						ptr = walkingQueueSize - 1;

					travelBackX[numTravelBackSteps] = walkingQueueX[ptr];
					travelBackY[numTravelBackSteps++] = walkingQueueY[ptr];
					dir = Misc.direction(walkingQueueX[ptr],
							walkingQueueY[ptr], firstX, firstY);
					if (lastDir != dir) {
						found = true;
						break;
					}

				} while (ptr != wQueueWritePtr);
			} else
				found = true;
			if (!found) {
				disconnected = true;
			} else {
				wQueueWritePtr = wQueueReadPtr;
				addToWalkingQueue(currentX, currentY);
				if (dir != -1 && (dir & 1) != 0) {
					for (int i = 0; i < numTravelBackSteps - 1; i++) {
						addToWalkingQueue(travelBackX[i], travelBackY[i]);
					}
					int wayPointX2 = travelBackX[numTravelBackSteps - 1], wayPointY2 = travelBackY[numTravelBackSteps - 1];
					int wayPointX1, wayPointY1;
					if (numTravelBackSteps == 1) {
						wayPointX1 = currentX;
						wayPointY1 = currentY;
					} else {
						wayPointX1 = travelBackX[numTravelBackSteps - 2];
						wayPointY1 = travelBackY[numTravelBackSteps - 2];
					}
					dir = Misc.direction(wayPointX1, wayPointY1, wayPointX2,
							wayPointY2);
					if (dir == -1 || (dir & 1) != 0) {
					} else {
						dir >>= 1;
						found = false;
						int x = wayPointX1, y = wayPointY1;
						while (x != wayPointX2 || y != wayPointY2) {
							x += Misc.directionDeltaX[dir];
							y += Misc.directionDeltaY[dir];
							if ((Misc.direction(x, y, firstX, firstY) & 1) == 0) {
								found = true;
								break;
							}
						}
						if (!found) {

						} else
							addToWalkingQueue(wayPointX1, wayPointY1);
					}
				} else {
					for (int i = 0; i < numTravelBackSteps; i++) {
						addToWalkingQueue(travelBackX[i], travelBackY[i]);
					}
				}
				for (int i = 0; i < newWalkCmdSteps; i++) {
					addToWalkingQueue(newWalkCmdX[i], newWalkCmdY[i]);
				}
			}
			isRunning = newWalkCmdIsRunning || isRunning;
		}
	    newWalkCmdSteps = 0;
	}
	
	public void parseIncomingPackets() {
		switch (packetManager().getPacket().getOpcode()) {

		case 53:
			break;
			
			
		case 236: // pickup item
			int itemY = packetManager().getInStream().readSignedWordBigEndian();
			int itemID = packetManager().getInStream().readUnsignedWord();
			int itemX = packetManager().getInStream().readSignedWordBigEndian();

			System.out.println("pickupItem: "+itemX+","+itemY+" itemID: "+itemID);

			break;

		case 3: // focus change
			break;
		case 86: // camera angle
			break;
		case 241: // mouse clicks
			break;

		case 214: // change item places
			int moveWindow = packetManager().getInStream().readSignedWordBigEndianA();
			getBankContainer().setInsertItems(packetManager().getInStream().readSignedByteC() == 1);
			int itemFrom = packetManager().getInStream().readSignedWordBigEndianA();
			int itemTo = packetManager().getInStream().readSignedWordBigEndian();
			if (ContainerType.BANK.getMoveWindow() == moveWindow) {
				getBankContainer().switchItems(itemFrom, itemTo);
			} else {
				getInventoryContainer().switchItems(itemFrom, itemTo);
			}

			break;

		case 41: // wear item
			int wearID = packetManager().getInStream().readUnsignedWord();
			int wearSlot = packetManager().getInStream().readUnsignedWordA();
			int interfaceID = packetManager().getInStream().readUnsignedWordA();
			getEquipment().wear(wearID, wearSlot);
			break;

		case 145: // remove item (opposite for wearing)
			interfaceID = packetManager().getInStream().readUnsignedWordA();
			int removeSlot = packetManager().getInStream().readUnsignedWordA();
			int removeID = packetManager().getInStream().readUnsignedWordA();
			//send(new SendMessage("RemoveItem: " + removeID + " ja " + interfaceID + " slot: " + removeSlot));

			if (interfaceID == 1688) {
				if (getEquipmentContainer().getItems()[removeSlot].getId() > 0)
					getEquipment().removeEquipment(removeID, removeSlot);
			} else if (interfaceID == 5064) {
				getBankContainer().addItem(new GameItem(removeID, 1), removeSlot);
			} else if (interfaceID == 5382) {
				getBankContainer().removeItem(new GameItem(removeID, 1), removeSlot);
			}
			break;

		case 117: // bank 5 items
			interfaceID = packetManager().getInStream().readUnsignedWordA();
			removeID = packetManager().getInStream().readSignedWordBigEndianA();
			removeSlot = packetManager().getInStream().readSignedWordBigEndian();
			// println_debug(interfaceID+"Bank 5 items: "+removeID+" ja slot: "+removeSlot);

			if (interfaceID == 18579) {
				getBankContainer().addItem(new GameItem(removeID, 5), removeSlot);
			} else if (interfaceID == 34453) {
				getBankContainer().removeItem(new GameItem(removeID, 5), removeSlot);
			}
			break;

		case 43: // bank 10 items
			interfaceID = packetManager().getInStream().readUnsignedWordA();
			removeID = packetManager().getInStream().readUnsignedWordA();
			removeSlot = packetManager().getInStream().readUnsignedWordA();
			// println_debug(interfaceID+"Bank 10 items: "+removeID+" ja slot: "+removeSlot);

			if (interfaceID == 51347) {
				getBankContainer().addItem(new GameItem(removeID, 10), removeSlot);
			} else if (interfaceID == 1685) {
				getBankContainer().removeItem(new GameItem(removeID, 10), removeSlot);
			}

			break;

		case 129: // bank All items
			removeSlot = packetManager().getInStream().readUnsignedWordA();
			interfaceID = packetManager().getInStream().readUnsignedWordA();
			removeID = packetManager().getInStream().readUnsignedWordA();
			// println_debug(interfaceID+"Bank All items: "+removeID+" ja slot: "+removeSlot);
			GameItem item = getInventoryContainer().getItems()[removeSlot];
			if (interfaceID == 4936) {
				getBankContainer().addItem(new GameItem(getInventoryContainer().getItems()[removeSlot].getId(), getInventoryContainer().getItemAmount(item, item.isStackable() ? true : false)), removeSlot);
			} else if (interfaceID == 5510) {
				getBankContainer().removeItem(getBankContainer().getItems()[removeSlot], removeSlot);
			}

			break;

		case 135: // bank X items Part1
			Stream out = new Stream(Unpooled.buffer(1), packetManager().getOutStreamDecryption());
			out.createFrame(27);
			send(out);
			bankXremoveSlot = packetManager().getInStream().readSignedWordBigEndian();
			bankXinterfaceID = packetManager().getInStream().readUnsignedWordA();
			bankXremoveID = packetManager().getInStream().readSignedWordBigEndian();
			// println_debug(bankXinterfaceID+"Bank X Items Part1: "+bankXremoveID+" ja slot: "+bankXremoveSlot);

			break;

		case 208: // bank X items Part2
			int bankXamount = packetManager().getInStream().readDWord();
			if (bankXinterfaceID == 5064) {
				getBankContainer().addItem(new GameItem(getInventoryContainer().getItems()[bankXremoveSlot].getId(), bankXamount), bankXremoveSlot);
			} else if (bankXinterfaceID == 5382) {
				getBankContainer().removeItem(new GameItem(getBankContainer().getItems()[bankXremoveSlot].getId(), bankXamount), bankXremoveSlot);
			}
			break;

		case 87: // drop item
			int droppedItem = packetManager().getInStream().readUnsignedWordA();
			@SuppressWarnings("unused")
			int someJunk = packetManager().getInStream().readUnsignedByte()
					+ packetManager().getInStream().readUnsignedByte();
			int slot = packetManager().getInStream().readUnsignedWordA();
			// println_debug("dropItem: "+droppedItem+" Slot: "+slot);
			dropItem(droppedItem, slot);
			break;
		case 40:
			send(new CloseInterface());
			break;
		case 185: // clicking most buttons
			getUpdateFlags().add(UpdateFlag.UPDATE);
			getUpdateFlags().add(UpdateFlag.APPEARANCE);
			int actionButtonId = Misc.HexToInt(packetManager().getInStream().buffer, 0, packetManager().getPacket().getSize());
			switch (actionButtonId) {
								
                            //Log out
                            case 9154:
								if(logoutDelay == 0) {
									send(new Logout());
								} else {
									send(new SendMessage("You need to wait "+logoutDelay/2+" seconds to logout!"));
								}
								break;						
						
								
							case 21011:
								getBankContainer().setNotesEnabled(false);
								break;
								
			// Emotions
			case 168:
				playAnimation(855, 0);
				break;
			case 169:
				playAnimation(856, 0);
				break;
			case 162:
				playAnimation(857, 0);
				break;
			case 164:
				playAnimation(858, 0);
				break;
			case 165:
				playAnimation(859, 0);
				break;
			case 161:
				playAnimation(860, 0);
				break;
			case 170:
				playAnimation(861, 0);
				break;
			case 171:
				playAnimation(862, 0);
				break;
			case 163:
				playAnimation(863, 0);
				break;
			case 167:
				playAnimation(864, 0);
				break;
			case 172:
				playAnimation(865, 0);
				break;
			case 166:
				playAnimation(866, 0);
				break;
			case 2154:
				playAnimation(1128, 0);
				break;
			case 2155:
				playAnimation(1131, 0);
				break;
			case 25103:
				playAnimation(1130, 0);
				break;
			case 25106:
				playAnimation(1129, 0);
				break;

			// Log out

			case 153:
				isRunning = true;
				break;

			case 152:
				isRunning = false;
				break;

			case 21241:
				createItems = !createItems;
				break;

			case 21010:
				getBankContainer().setNotesEnabled(true);
				break;

			default:
				// System.out.println("Player stands in: X="+absX+" Y="+absY);
				System.out.println("Action Button: " + actionButtonId);
				break;
			}
			break;

		case 226:
		case 78:
		case 148:
		case 183:
		case 230:
		case 136:
		case 189:
		case 152:
		case 200:
		case 85:
		case 165:
		case 238:
		case 150:
		case 36:
		case 246:
		case 77:
			break;

		// any packets we might have missed
		default:
			//System.out.println("packet: " + packetType);
			break;
		}
	}
	

    public int walkDir = -1;
    public int runDir = -1;

	private int bankXremoveSlot = 0;
	private int bankXinterfaceID = 0;
	@SuppressWarnings("unused")
	private int bankXremoveID = 0;

	public int lowMemoryVersion = 0;
	public int logoutDelay = 0;
	
	/**
	 * The interfaces of leveling.
	 */
	private int[][] levelUpInterfaces = { { 6247, 6248, 6249 },
			{ 6253, 6254, 6255 }, { 6206, 6207, 6208 }, { 6216, 6217, 6218 },
			{ 4443, 5453, 6114 }, { 6242, 6243, 6244 }, { 6211, 6212, 6213 },
			{ 6226, 6227, 6228 }, { 4272, 4273, 4274 }, { 6231, 6232, 6233 },
			{ 6258, 6259, 6260 }, { 4282, 4283, 4284 }, { 6263, 6264, 6265 },
			{ 6221, 6222, 6223 }, { 4416, 4417, 4438 }, { 6237, 6238, 6239 },
			{ 4277, 4278, 4279 }, { 4261, 4263, 4264 },
			{ 12122, 12123, 12124 }, { 13929, 13930, 13931 },
			{ 4267, 4268, 4269 } };

	/**
	 * Gets the level up interfaces.
	 * 
	 * @return
	 */
	public int[][] getLevelUpInterfaces() {
		return levelUpInterfaces;
	}

	public void dropItem(int droppedItem, int slot) {
		if(getInventoryContainer().getItems()[slot].getAmount() != 0 && droppedItem != -1 && getInventoryContainer().getItems()[slot].getId() == droppedItem) {
			ItemHandler.addItem(getInventoryContainer().getItems()[slot].getId(), getLocation().getX(), getLocation().getY(), getInventoryContainer().getItems()[slot].getAmount(), getEntityId(), false);
			getInventoryContainer().removeItem(getInventoryContainer().getItems()[slot], slot);
			getUpdateFlags().add(UpdateFlag.UPDATE);
		}
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public HashMap<Integer, String> levelup = null;
	
}