package com.zlidder.model.object;

import com.zlidder.io.cache.def.ObjectDefinition;
import com.zlidder.model.Location;

public class GameObject {

	private int id;
	private Location location;
	private ObjectDefinition definitions;
	
	public GameObject(int id, Location location) {
		this.id = id;
		this.location = location;
		this.definitions = ObjectDefinition.getDefinitions()[id];
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}

	public ObjectDefinition getDefinitions() {
		return definitions;
	}

	public void setDefinitions(ObjectDefinition definitions) {
		this.definitions = definitions;
	}
	
}
