package com.zlidder.model.npc;

public class NpcList {
	public int npcId;
	public String npcName;
	public int npcCombat;
	public int npcHealth;
	public int attack = 0;
	public int defence = 0;
	public int maxHit = 0;
	public int atkanim = 0x326;
	public int blockanim = 404;
	public int dieanim = 2304;
	public int attackSound = 0;
	public int blockSound = 0;
	public int dieSound = 0;
	
	public NpcList(int npcId) {
		this.npcId = npcId;
	}
}
