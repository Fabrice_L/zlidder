package com.zlidder.model.npc;

import com.zlidder.io.cache.def.NpcDefinition;
import com.zlidder.model.Entity;
import com.zlidder.model.Location;
import com.zlidder.model.UpdateFlag;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;
import com.zlidder.world.NpcHandler;

public class Npc extends Entity{

	public int makeX, makeY, maxHit, defence, attack, moveX, moveY, walkingType;
	public int spawnX, spawnY;
    public int viewX, viewY;
	/**
	 * attackType: 0 = melee, 1 = range, 2 = mage
	 */
	public boolean isDead;
	public int freezeTimer;
	public boolean randomWalk;
	public String forcedText;
	
	public Npc(int entitySlot, int npcType) {
		setEntityId(entitySlot);
		npcDefinition = NpcDefinition.getDefinitions()[npcType];
		setPrimaryDirection(-1);
		isDead = false;
		randomWalk = true;		
	}

	/**
	* Text update
	**/
	
	public void forceChat(String text) {
		forcedText = text;
		getUpdateFlags().add(UpdateFlag.CHAT);
		getUpdateFlags().add(UpdateFlag.UPDATE);
	}
	
	/**
	*
	Face
	*
	**/
	
	public int FocusPointX = -1, FocusPointY = -1;
	
	public void appendSetFocusDestination(Stream str) {
        str.writeWordBigEndian(FocusPointX);
        str.writeWordBigEndian(FocusPointY);
    }
	
	public void turnNpc(int i, int j) {
        FocusPointX = 2 * i + 1;
        FocusPointY = 2 * j + 1;
		getUpdateFlags().add(UpdateFlag.FACE);
        getUpdateFlags().add(UpdateFlag.UPDATE);
    }

	public void appendFaceToUpdate(Stream str) {
			str.writeWordBigEndian(viewX);
			str.writeWordBigEndian(viewY);
	}

	public void clearUpdateFlags() {
		getUpdateFlags().remove(UpdateFlag.UPDATE);
		getUpdateFlags().remove(UpdateFlag.CHAT);
		getUpdateFlags().remove(UpdateFlag.ANIMATION);
		getUpdateFlags().remove(UpdateFlag.APPEARANCE);
		getUpdateFlags().remove(UpdateFlag.FACE);
		getUpdateFlags().remove(UpdateFlag.HIT);
		getUpdateFlags().remove(UpdateFlag.FACE);
		getUpdateFlags().remove(UpdateFlag.GRAPHIC);
		hitUpdateRequired2 = false;
		forcedText = null;
		moveX = 0;
		moveY = 0;
		setPrimaryDirection(-1);
		FocusPointX = -1;
		FocusPointY = -1;
	}

	
	public int getNextWalkingDirection() {
		int dir;
		dir = Misc.direction(getLocation().getX(), getLocation().getY(), (getLocation().getX() + moveX), (getLocation().getY() + moveY));
		if(dir == -1) return -1;
		dir >>= 1;
		setLocation(new Location(getLocation().getX() + moveX, getLocation().getY() + moveY));
		return dir;
	}

	public void getNextNPCMovement(int i) {
		setPrimaryDirection(-1);
		if(NpcHandler.npcs[i].freezeTimer == 0) {
			setPrimaryDirection(getNextWalkingDirection());
		}
	}
	
	public int hitDiff2 = 0;
	public boolean hitUpdateRequired2 = false;
	
	public void appendHitUpdate2(Stream str) {		
		if (getCurrentHitpoints() <= 0) {
			isDead = true;
		}
		str.writeByteA(hitDiff2); 
		if (hitDiff2 > 0) {
			str.writeByteC(1); 
		} else {
			str.writeByteC(0); 
		}	
		str.writeByteA(getCurrentHitpoints()); 
		str.writeByte(getMaxHitpoints()); 	
	}
	
	public void handleHitMask(int damage) {
		if (!getUpdateFlags().contains(UpdateFlag.HIT)) {
			setHitDiff(damage);
			if (getHitDiff() >= getCurrentHitpoints()) {
				setHitDiff(getCurrentHitpoints());
			}
			setCurrentHitpoints(getCurrentHitpoints() - getHitDiff());
			getUpdateFlags().add(UpdateFlag.HIT);
		} else if (!hitUpdateRequired2) {
			hitUpdateRequired2 = true;
			hitDiff2 = damage;		
		}
		getUpdateFlags().add(UpdateFlag.UPDATE);
	}
	
	NpcDefinition npcDefinition;
	
	public NpcDefinition getNpcDefinition() {
		return npcDefinition;
	}
}
