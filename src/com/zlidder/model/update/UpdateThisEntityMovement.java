package com.zlidder.model.update;

import com.zlidder.model.Entity;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.UpdatingBlock;
import com.zlidder.model.player.Player;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;

public class UpdateThisEntityMovement extends UpdatingBlock {

	@Override
	public void update(Entity entity, Stream str) {
		if (entity instanceof Player) {
			Player player = (Player) entity;
			if (player.getUpdateFlags().contains(UpdateFlag.TELEPORT) || player.getUpdateFlags().contains(UpdateFlag.MAPREGION)) {
				str.createFrameVarSizeWord(81);
				str.initBitAccess();
				str.writeBits(1, 1);
				str.writeBits(2, 3); // updateType
				str.writeBits(2, player.getLocation().getHeight());
				str.writeBits(1, player.getUpdateFlags().contains(UpdateFlag.TELEPORT) ? 1 : 0); // set to true, if discarding (clientside)
										// walking queue
				str.writeBits(1, player.getUpdateFlags().contains(UpdateFlag.UPDATE) ? 1 : 0);
				str.writeBits(7, player.currentY);
				str.writeBits(7, player.currentX);
			} else {
				if (player.getPrimaryDirection() == -1) {
					// don't have to update the character position, because we're just
					// standing
					str.createFrameVarSizeWord(81);
					str.initBitAccess();
					if (player.getUpdateFlags().contains(UpdateFlag.UPDATE)) {
						// tell client there's an update block appended at the end
						str.writeBits(1, 1);
						str.writeBits(2, 0);
					} else {
						str.writeBits(1, 0);
					}
				} else {
					str.createFrameVarSizeWord(81);
					str.initBitAccess();
					if (player.getSecondaryDirection() == -1) {
						// send "walking packet"
						str.writeBits(1, 1);
						str.writeBits(2, 1); // updateType
						str.writeBits(3, Misc.xlateDirectionToClient[player.getPrimaryDirection()]);
						str.writeBits(1, player.getUpdateFlags().contains(UpdateFlag.UPDATE) ? 1 : 0); // tell client there's an update								// block appended at the end
					} else {
						// send "running packet"
						str.writeBits(1, 1);
						str.writeBits(2, 2); // updateType
						str.writeBits(3, Misc.xlateDirectionToClient[player.getPrimaryDirection()]);
						str.writeBits(3, Misc.xlateDirectionToClient[player.getSecondaryDirection()]);
						str.writeBits(1, player.getUpdateFlags().contains(UpdateFlag.UPDATE) ? 1 : 0);
					}
				}
			}
		}
	}

}
