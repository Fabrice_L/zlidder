package com.zlidder.model.update;

import com.zlidder.model.Entity;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.UpdatingBlock;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.Player;
import com.zlidder.util.Stream;

public class UpdateEntityState extends UpdatingBlock {

	@Override
	public void update(Entity entity, Stream str) {
		if (entity instanceof Player) {
			Player player = (Player) entity;
			if (!player.getUpdateFlags().contains(UpdateFlag.UPDATE) && !player.getUpdateFlags().contains(UpdateFlag.CHAT)) {
				return;
			} // nothing required
			
			int updateMask = 0;
			if (player.getUpdateFlags().contains(UpdateFlag.GRAPHIC)) {
				updateMask |= 0x100;
			}
			if (player.getUpdateFlags().contains(UpdateFlag.ANIMATION)) {
				updateMask |= 0x8;
			}
			if (player.getUpdateFlags().contains(UpdateFlag.CHAT)) {
				updateMask |= 0x80;
			}
			if (player.getUpdateFlags().contains(UpdateFlag.FACE)) {
				updateMask |= 1;
			}
			if (player.getUpdateFlags().contains(UpdateFlag.APPEARANCE)) {
				updateMask |= 0x10;
			}
			if (player.getUpdateFlags().contains(UpdateFlag.HIT)) {
				updateMask |= 0x200;
			}
			if (player.FocusPointX != -1) {
				updateMask |= 2;
			}
	
			if (updateMask >= 0x100) {
				// byte isn't sufficient
				updateMask |= 0x40; // indication for the client that updateMask is
									// stored in a word
				str.writeByte(updateMask & 0xFF);
				str.writeByte(updateMask >> 8);
			} else {
				str.writeByte(updateMask);
			}
			
			if (player.getUpdateFlags().contains(UpdateFlag.GRAPHIC)) {
				player.appendGraphicUpdate(str);
			}
			if (player.getUpdateFlags().contains(UpdateFlag.ANIMATION)) {
				player.appendAnimUpdate(str);
			}
			if (player.getUpdateFlags().contains(UpdateFlag.CHAT)) {
				str.writeWordBigEndian(((player.chatTextColor & 0xFF) << 8)
						+ (player.chatTextEffects & 0xFF));
				str.writeByte(player.playerRights);
				str.writeByteC(player.chatTextSize); // no more than 256 bytes!!!
				str.writeBytes_reverse(player.chatText, player.chatTextSize, 0);
				player.getUpdateFlags().remove(UpdateFlag.CHAT);
			}
			if (player.getUpdateFlags().contains(UpdateFlag.FACE)) {
				player.appendFaceEntity(str);
			}
			if (player.getUpdateFlags().contains(UpdateFlag.APPEARANCE)) {
				player.appendPlayerAppearance(str);
			}
			if (player.getUpdateFlags().contains(UpdateFlag.HIT)) {
				player.appendHitUpdate(str);
			}
			if (player.FocusPointX != -1) {
				str.writeWordBigEndianA(player.FocusPointX);
				str.writeWordBigEndian(player.FocusPointY);
				player.FocusPointX = -1;
				player.FocusPointY = -1;
			}
		}
		
		if (entity instanceof Npc) {
			Npc npc = (Npc) entity;
			if(!npc.getUpdateFlags().contains(UpdateFlag.UPDATE)) {
				return ;		
			}
			
			int updateMask = 0;
			if (npc.getUpdateFlags().contains(UpdateFlag.ANIMATION)) {
				updateMask |= 0x10;
			}
			if(npc.hitUpdateRequired2) {
				updateMask |= 8;
			}
			if(npc.getUpdateFlags().contains(UpdateFlag.GRAPHIC)) {
				updateMask |= 0x80;
			}
			if(npc.getUpdateFlags().contains(UpdateFlag.FACE)) {
				updateMask |= 0x20;
			}
			if(npc.getUpdateFlags().contains(UpdateFlag.CHAT)) {
				updateMask |= 1;
			}
			if(npc.getUpdateFlags().contains(UpdateFlag.HIT)) {
				updateMask |= 0x40;		
			}
			if(npc.FocusPointX != -1) {
				updateMask |= 4;		
			}	
			str.writeByte(updateMask);
					
			if (npc.getUpdateFlags().contains(UpdateFlag.ANIMATION)) {
				npc.appendAnimUpdate(str);
			}
			if (npc.hitUpdateRequired2) {
				npc.appendHitUpdate2(str);
			}
			if (npc.getUpdateFlags().contains(UpdateFlag.GRAPHIC)) {
				npc.appendGraphicUpdate(str);
			}
			if (npc.getUpdateFlags().contains(UpdateFlag.FACE)) {
				npc.appendFaceEntity(str);
			}
			if(npc.getUpdateFlags().contains(UpdateFlag.CHAT)) {
				str.writeString(npc.forcedText);
			}
			if (npc.getUpdateFlags().contains(UpdateFlag.HIT)) {
				npc.appendHitUpdate(str);
			}
			if(npc.FocusPointX != -1) {
				npc.appendSetFocusDestination(str);
			}
		}
	}
}
