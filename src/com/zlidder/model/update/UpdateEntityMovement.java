package com.zlidder.model.update;

import com.zlidder.model.Entity;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.UpdatingBlock;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.Player;
import com.zlidder.util.Misc;
import com.zlidder.util.Stream;

public class UpdateEntityMovement extends UpdatingBlock {

	@Override
	public void update(Entity entity, Stream str) {
		if (entity instanceof Player) {
			Player player = (Player) entity;
			if (player.getPrimaryDirection() == -1) {
				// don't have to update the character position, because the char is
				// just standing
				if (player.getUpdateFlags().contains(UpdateFlag.UPDATE) || player.getUpdateFlags().contains(UpdateFlag.CHAT)) {
					// tell client there's an update block appended at the end
					str.writeBits(1, 1);
					str.writeBits(2, 0);
				} else
					str.writeBits(1, 0);
			} else if (player.getSecondaryDirection() == -1) {
				// send "walking packet"
				str.writeBits(1, 1);
				str.writeBits(2, 1);
				str.writeBits(3, Misc.xlateDirectionToClient[player.getPrimaryDirection()]);
				str
						.writeBits(1,
								(player.getUpdateFlags().contains(UpdateFlag.UPDATE) || player.getUpdateFlags().contains(UpdateFlag.CHAT)) ? 1 : 0);
			} else {
				// send "running packet"
				str.writeBits(1, 1);
				str.writeBits(2, 2);
				str.writeBits(3, Misc.xlateDirectionToClient[player.getPrimaryDirection()]);
				str.writeBits(3, Misc.xlateDirectionToClient[player.getSecondaryDirection()]);
				str
						.writeBits(1,
								(player.getUpdateFlags().contains(UpdateFlag.UPDATE) || player.getUpdateFlags().contains(UpdateFlag.CHAT)) ? 1 : 0);
			}
		}
		
		if (entity instanceof Npc) {
			Npc npc = (Npc) entity;
			if (npc.getPrimaryDirection() == -1) {
				
				if (npc.getUpdateFlags().contains(UpdateFlag.UPDATE)) {
					
					str.writeBits(1, 1);
					str.writeBits(2, 0);
				} else {
					str.writeBits(1, 0);
				}
			} else {
				
				str.writeBits(1, 1);
				str.writeBits(2, 1);		
				str.writeBits(3, Misc.xlateDirectionToClient[npc.getPrimaryDirection()]);
				if (npc.getUpdateFlags().contains(UpdateFlag.UPDATE)) {
					str.writeBits(1, 1);		
				} else {
					str.writeBits(1, 0);
				}
			}
		}
	}

}
