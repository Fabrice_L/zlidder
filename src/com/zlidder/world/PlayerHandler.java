/**
 * A simple 194 server.
 * @author Phych0k
 *
 */
package com.zlidder.world;



import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.zlidder.model.object.GameObject;
import com.zlidder.model.player.Player;

public class PlayerHandler{
	private static List<GameObject> objects = new LinkedList<GameObject>();

	private static final Queue<Player> queuedLogins = new ConcurrentLinkedQueue<Player>();
	
	public static final int maxPlayers = 512;
	public static Player players[] = new Player[maxPlayers];
	public static int playerSlotSearchStart = 1;			// where we start searching at when adding a new player
	public static int playerCount=0;
	public static String playersCurrentlyOn[] = new String[maxPlayers];
	public static int clientCount = 0;
	public PlayerHandler()
	{
		for(int i = 0; i < maxPlayers; i++) {
			players[i] = null;
		}
	}
	
	public static int getFreeSlot() {
		int slot = -1, i = 1;
		do {
			if(players[i] == null) {
				slot = i;
				break;
			}
			i++;
			if(i >= maxPlayers) i = 0;		// wrap around
		} while(i <= maxPlayers);
		return slot;
	}
	public static void addClient(int slot, Player player) {
		if(player == null) return;
		players[slot] = player;
		clientCount += 1;
		playerSlotSearchStart = slot + 1;
		if(playerSlotSearchStart > maxPlayers) playerSlotSearchStart = 1;
	}
		// a new client connected

	public static int getPlayerCount()
	{
		return playerCount;
	}

	public static void updatePlayerNames(){
		playerCount=0;
		for(int i = 0; i < maxPlayers; i++) {
			if(players[i] != null)
			{
				playersCurrentlyOn[i] = players[i].getPlayerName();
				playerCount++;
			}
			else
				playersCurrentlyOn[i] = "";
		}
	}

	public static boolean isPlayerOn(String playerName)
	{
		for(int i = 0; i < maxPlayers; i++) {
			if(playersCurrentlyOn[i] != null){
				if(playersCurrentlyOn[i].equalsIgnoreCase(playerName)) return true;
			}
		}
		return false;
	}

	public static void removePlayer(Player plr)
	{
		// anything can be done here like unlinking this player structure from any of the other existing structures
		plr.destruct();
	}

	public static List<GameObject> getObjects() {
		return objects;
	}
	
	public static void queueLogin(Player player) {
		getQueuedlogins().add(player);
	}

	public static Queue<Player> getQueuedlogins() {
		return queuedLogins;
	}
	
}
