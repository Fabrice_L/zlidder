package com.zlidder.world;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.zlidder.model.Location;
import com.zlidder.model.Region;
import com.zlidder.model.UpdateFlag;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.npc.NpcList;
import com.zlidder.util.Misc;

public class NpcHandler {
	public static int maxNPCs = 10000;
	public static int maxListedNPCs = 10000;
	public static Npc npcs[] = new Npc[maxNPCs];
	public static NpcList NpcList[] = new NpcList[maxListedNPCs];
	


	public NpcHandler() {
		for(int i = 0; i < maxNPCs; i++) {
			npcs[i] = null;
		}
		for(int i = 0; i < maxListedNPCs; i++) {
			NpcList[i] = null;
		}

		loadAutoSpawn("./config/npcSpawn.cfg");
	}




	@SuppressWarnings("resource")
	public boolean loadAutoSpawn(String FileName) {
		String line = "";
		String token = "";
		String token2 = "";
		String token2_2 = "";
		String[] token3 = new String[10];
		boolean EndOfFile = false;
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./"+FileName));
		} catch(FileNotFoundException fileex) {
			Misc.println(FileName+": file not found.");
			return false;
		}
		try {
			line = characterfile.readLine();
		} catch(IOException ioexception) {
			Misc.println(FileName+": error loading file.");
			return false;
		}
		while(EndOfFile == false && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("spawn")) {
					if(Integer.parseInt(token3[0]) <= 5691) {
						NpcHandler.newNPC(Integer.parseInt(token3[0]) , Integer.parseInt(token3[1]), Integer.parseInt(token3[2]), Integer.parseInt(token3[3]), Integer.parseInt(token3[4]), getNpcListHP(Integer.parseInt(token3[0])));
					}
				
				}
			} else {
				if (line.equals("[ENDOFSPAWNLIST]")) {
					try { characterfile.close(); } catch(IOException ioexception) { }
					return true;
				}
			}
			try {
				line = characterfile.readLine();
			} catch(IOException ioexception1) { EndOfFile = true; }
		}
		try { characterfile.close(); } catch(IOException ioexception) { }
		return false;
	}


	public static void newNPC(int npcType, int x, int y, int heightLevel, int WalkingType, int HP) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}

		if(slot == -1) return;

		Npc newNPC = new Npc(slot, npcType);
		newNPC.setLocation(new Location(x, y, heightLevel));
		newNPC.walkingType = WalkingType;
		newNPC.getUpdateFlags().add(UpdateFlag.FACE);
		newNPC.getUpdateFlags().add(UpdateFlag.UPDATE);
		newNPC.setCurrentHitpoints(HP);
		newNPC.setMaxHitpoints(HP);
		npcs[slot] = newNPC;
		
		for (Region region : RegionHandler.getRegions()) {
			if (region.getRegionX() == newNPC.getLocation().getX()/64 
					&& region.getRegionY() == newNPC.getLocation().getY()/64) {
				region.addEntity(newNPC);
				newNPC.setRegion(region);
			}
		}
	}


	public int getNpcListHP(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].npcHealth;
				}
			}
		}
		return 10;
	}

	public int getNpcListMaxHit(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].maxHit;
				}
			}
		}
		return 0;
	}
	
	public int getNpcListAttack(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].attack;
				}
			}
		}
		return 0;
	}
	
	public int getNpcListDefence(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].defence;
				}
			}
		}
		return 0;
	}
	
	public String getNpcListName(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].npcName.replace("_", " ");
				}
			}
		}
		return "nothing";
	}
	
	public static int getAttackEmote(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].atkanim;
				}
			}
		}
		return 0;	
	}
	
	public static int getBlockEmote(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].blockanim;
				}
			}
		}
		return 0;	
	}
	
	public static int getDeadEmote(int npcId) {
		for (int i = 0; i < maxListedNPCs; i++) {
			if (NpcList[i] != null) {
				if (NpcList[i].npcId == npcId) {
					return NpcList[i].dieanim;
				}
			}
		}
		return 0;	
	}

}
