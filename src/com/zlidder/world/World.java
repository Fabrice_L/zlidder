package com.zlidder.world;

import com.zlidder.model.pulse.Pulse;
import com.zlidder.model.pulse.PulseManager;

public class World {

	private static PulseManager pulseManager = new PulseManager();

	public static void submit(final Pulse pulse) {
		World.pulseManager.submit(pulse);
	}
	
	public static PulseManager getPulseManager() {
		return pulseManager;
	}
}
