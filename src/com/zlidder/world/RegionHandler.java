package com.zlidder.world;

import java.util.LinkedList;
import java.util.List;

import com.zlidder.model.Region;

public class RegionHandler {

    private static List<Region> regions = new LinkedList<Region>();
    
    public static void loadRegions() {
    	long k = System.currentTimeMillis();
    	for (int i = 0; i <= 156; i++) {
    		for (int j = 0; j <= 156; j++)
    			regions.add(new Region(i, j));
    	}
    	System.out.println("Regions took " + (System.currentTimeMillis() - k) + "ms to load.");
    }
    
    public static List<Region> getRegions() {
    	return regions;
    }
	
}