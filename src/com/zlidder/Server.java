package com.zlidder;

import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.zlidder.io.cache.IndexedFileSystem;
import com.zlidder.io.cache.decoder.ItemDefinitionDecoder;
import com.zlidder.io.cache.decoder.NpcDefinitionDecoder;
import com.zlidder.io.cache.decoder.ObjectDefinitionDecoder;
import com.zlidder.io.cache.map.MapIndexDecoder;
import com.zlidder.io.cache.map.WorldObjectsDecoder;
import com.zlidder.jobs.JobScheduler;
import com.zlidder.jobs.world.ProcessGameJob;
import com.zlidder.net.ChannelHandler;
import com.zlidder.net.codecs.Encoder;
import com.zlidder.net.codecs.LoginDecoder;
import com.zlidder.script.ScriptManager;
import com.zlidder.world.NpcHandler;
import com.zlidder.world.RegionHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Server implements Runnable{
	
	private static final Logger logger = Logger.getLogger(Server.class.getName());
	
	private final int port = 43595;
	
	public static void main(String args[]) {
		new Thread(new Server()).start();
		
		try {
			new JobScheduler();
			JobScheduler.scheduleAtFixedRate(600, ProcessGameJob.class);
			IndexedFileSystem fileSystem = new IndexedFileSystem(Paths.get("data/cache"), true);
			new NpcDefinitionDecoder(fileSystem).run();
			new ItemDefinitionDecoder(fileSystem).run();
			new ObjectDefinitionDecoder(fileSystem).run();
			new MapIndexDecoder(fileSystem).run();
			new WorldObjectsDecoder(fileSystem).run();
			RegionHandler.loadRegions();
			ScriptManager.loadScripts();
			new NpcHandler();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(){		
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		
		try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .childHandler(new ChannelInitializer<SocketChannel>() {
                 @Override
                 public void initChannel(SocketChannel ch) throws Exception {
             		ch.pipeline().addLast("encoder", new Encoder());
            		ch.pipeline().addLast("decoder", new LoginDecoder());
            		ch.pipeline().addLast("handler", new ChannelHandler());
                 }
             })
             .option(ChannelOption.SO_BACKLOG, 128)          // (5)
             .childOption(ChannelOption.SO_KEEPALIVE, true); // (6)

            ChannelFuture f = b.bind(port).sync(); // (7)
          
            logger.log(Level.INFO, Config.SERVER_NAME + " started on port: " + port);
            
            f.channel().closeFuture().sync();
            
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
