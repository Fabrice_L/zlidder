package com.zlidder.io;

import com.zlidder.model.player.Player;
import com.zlidder.model.player.skills.Skills;
import com.zlidder.net.packet.outgoing.ChatBoxInterface;
import com.zlidder.net.packet.outgoing.SendMessage;
import com.zlidder.net.packet.outgoing.SendString;

/**
 * The action
 * 
 * @author Simon Dexter
 * 
 */
public class ActionSender {

	/**
	 * The client instance.
	 */
	private Player client;


	
	/**
	 * Creates the actionsender for this client only.
	 * 
	 * @param client
	 *            The client.
	 */
	public ActionSender(Player client) {
		this.client = client;
	}


	/**
	 * Sends the level up.
	 * 
	 * @param skillId
	 *            The skill id.
	 * @return The action.
	 */
	public ActionSender sendLevelUp(int skillId) {
		client.send(new SendMessage("Congratulations! You are now level "
				+ client.getSkills().getLevelForXP(skillId) + " "
				+ Skills.SKILL_NAME[skillId][1] + "."));
		client.send(new ChatBoxInterface(client.getLevelUpInterfaces()[skillId][0]));
		client.send(new SendString("@dbl@Congratulations, you just advanced "
				+ Skills.SKILL_NAME[skillId][0] + " level.",
				client.getLevelUpInterfaces()[skillId][1]));
		client.send(new SendString("Your " + Skills.SKILL_NAME[skillId][1] + " level is now "
				+ client.getSkills().getLevelForXP(skillId) + ".",
				client.getLevelUpInterfaces()[skillId][2]));
		return this;
	}

}
