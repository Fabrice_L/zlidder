package com.zlidder.io.cache.map;
import java.util.List;
import java.util.Map;

import com.zlidder.io.cache.IndexedFileSystem;
import com.zlidder.model.Location;
import com.zlidder.model.object.GameObject;
import com.zlidder.world.PlayerHandler;

/**
 * A decoder which decodes {@link MapObject}s and registers them with the game world.
 */
public final class WorldObjectsDecoder implements Runnable {
	/**
	 * The IndexedFileSystem.
	 */
	private final IndexedFileSystem fs;



	/**
	 * Create a new {@link WorldObjectsDecoder}.
	 *
	 * @param fs The {@link IndexedFileSystem} to load object files from.
	 */
	public WorldObjectsDecoder(IndexedFileSystem fs) {
		this.fs = fs;
	}

	/**
	 * Decode the {@code MapObject}s from the cache and register them with the world.
	 */
	@Override
	public void run() {
		Map<Integer, MapIndex> mapIndices = MapIndex.getIndices();
		try {
			int i = 0;
			for (MapIndex index : mapIndices.values()) {
				MapObjectsDecoder decoder = MapObjectsDecoder.create(fs, index);
				List<MapObject> objects = decoder.decode();

				int mapX = index.getX(), mapY = index.getY();
				for (MapObject object : objects) {
					i++;
					Location position = new Location(mapX + object.getLocalX(), mapY + object.getLocalY(),
						object.getHeight());

					//System.out.println(position.getX() + ":" + position.getY());
					GameObject gameObject = new GameObject(object.getId(), new Location(position.getX(), position.getY()));
					PlayerHandler.getObjects().add(gameObject);
				}
			}
			System.out.println("Loaded " + i + " world objects!");
		} catch (Exception ex) {
		}
	}
}