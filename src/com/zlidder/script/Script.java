package com.zlidder.script;

import java.io.File;

/**
 * @author Fabrice L
 * 
 * Represents a script file
 * 
 * 
 */
public class Script {

	private String scriptName;
	
	private String scriptDescription;
	
	private String scriptTrigger;
	
	private String scriptFile;
	
	private File path;
	
	public void setPath(File path) {
		this.path = path;
	}
	
	public String getName() {
		return scriptName;
	}
	
	public String getDescription() {
		return scriptDescription;
	}
	
	public String getTrigger() {
		return scriptTrigger;
	}
	
	public String getFileName() {
		return scriptFile;
	}
	
	public File getPath() {
		return path;
	}
	
}