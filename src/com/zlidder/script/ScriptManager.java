package com.zlidder.script;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author Fabrice L
 * 
 * Manages the scripts that are used by the server
 *
 */
public class ScriptManager {

	private static ArrayList<Script> scripts = new ArrayList<>();

	private static ScriptEngineManager engineManager = new ScriptEngineManager();

	public static void loadScripts() {
		if (scripts != null || !scripts.isEmpty())
			scripts.clear();

		fileList(new File("data/scripts/"));
		System.out.println("Finished Loading Scripts.");
	}

	public static void runScript(String trigger, Object... args) {
		System.out.println("Trying to run: " + trigger);
		for (Script script : scripts) {
			if (script.getTrigger().equalsIgnoreCase(trigger)) {
				try {
					ScriptEngine js = engineManager.getEngineByName("nashorn");
					js.eval(new FileReader(script.getPath() + "/" + script.getFileName()));
					Invocable invoke = (Invocable) js;
					invoke.invokeFunction("start", args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}	

	private static void fileList(File dir) {
		File [] files = dir.listFiles();

		for (File f : files) {
			if (f.isDirectory()) {
				fileList(f);
			} else if (f.isFile() && f.getName().endsWith(".json")){
				try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
					Gson gson = new GsonBuilder().create();
					Script script = gson.fromJson(reader, Script.class);
					script.setPath(new File(f.getParent()));
					scripts.add(script);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}