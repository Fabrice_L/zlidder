package com.zlidder.jobs;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;



/**
 * Created by fabrice on 6-6-2015.
 */

public class JobScheduler {

	private static Scheduler scheduler;
	
    public JobScheduler() throws SchedulerException{
        scheduler = new StdSchedulerFactory("quartz.properties").getScheduler();     
        scheduler.start();
    }
    
    public static void scheduleAtFixedRate(int milliSeconds, Class<? extends Job> object) throws SchedulerException {

        JobDetail job = JobBuilder.newJob(object).build();
        
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(
        		SimpleScheduleBuilder
        		.simpleSchedule()
        		.withIntervalInMilliseconds(milliSeconds)
        		.repeatForever()).build();
        
        scheduler.scheduleJob(job, trigger);
    }
  
}
