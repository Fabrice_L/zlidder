package com.zlidder.jobs.world;

import java.util.Iterator;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.zlidder.model.player.Player;
import com.zlidder.model.pulse.Pulse;
import com.zlidder.net.packet.outgoing.InitializePlayer;
import com.zlidder.net.packet.outgoing.UpdateNpc;
import com.zlidder.net.packet.outgoing.UpdatePlayer;
import com.zlidder.util.Misc;
import com.zlidder.world.ItemHandler;
import com.zlidder.world.NpcHandler;
import com.zlidder.world.PlayerHandler;
import com.zlidder.world.World;

@DisallowConcurrentExecution
public class ProcessGameJob implements Job {
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
	    try {
			Player plr = null;
			while ((plr = PlayerHandler.getQueuedlogins().poll()) != null) {
				plr.login();
			}
			
  			Iterator<Pulse> pulse = World.getPulseManager().getPulses().iterator();
			while(pulse.hasNext()) {
				Pulse p = pulse.next();
				p.cycle();
				if(!p.isRunning()) {
					pulse.remove();
				}
			}
			
			PlayerHandler.updatePlayerNames();
	
			// at first, parse all the incoming data
			// this has to be seperated from the outgoing part because we have to keep all the player data
			// static, so each Client gets exactly the same and not the one Clients are ahead in time
			// than the other ones.
			for (int i = 0; i < PlayerHandler.players.length; i++) {
				Player player = PlayerHandler.players[i];
				try {
					if (player == null || !player.isActive) {
						continue;
					}
					
					player.packetManager().processQueuedPackets();
					player.postProcessing();
					player.getNextPlayerMovement();
					
					if (player.disconnected) {
						PlayerHandler.removePlayer(player);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

	
			// loop through all players and do the updating stuff
			for(int i = 0; i < PlayerHandler.players.length; i++) {
				Player player = PlayerHandler.players[i];
				if(player == null || !player.isActive) {
					continue;
				}
	
				if(player.disconnected) {
					//save game here
					PlayerHandler.removePlayer(player);
				}
				else {
					if(!player.initialized) {
						player.send(new InitializePlayer());
						player.initialized = true;
					} else {
			            player.send(new UpdatePlayer());
			            player.send(new UpdateNpc());
			          }
				}
			}
	
			// post processing
			for(int i = 0; i < PlayerHandler.players.length; i++) {
				Player player = PlayerHandler.players[i];
				if(player == null || !player.isActive) {
					continue;
				}
				player.clearUpdateFlags();
			}
	    	    	
			for (int i = 0; i <= 5000; i++) {
				if (ItemHandler.globalItemID[i] != 0)
					ItemHandler.globalItemTicks[i]++;
	
				if ((ItemHandler.hideItemTimer + ItemHandler.showItemTimer) == ItemHandler.globalItemTicks[i]) {
					// Misc.println("Item Removed ["+i+"] id is ["+globalItemID[i]+"]");
					if (!ItemHandler.globalItemStatic[i]) {
						ItemHandler.removeItemAll(ItemHandler.globalItemID[i], ItemHandler.globalItemX[i],
								ItemHandler.globalItemY[i]);
					} else {
						Misc.println("Item is static");
					}
				}
	
				if (ItemHandler.showItemTimer == ItemHandler.globalItemTicks[i]) { // Phate: Item has
															// expired, show to all
					if (!ItemHandler.globalItemStatic[i]) {
						ItemHandler.createItemAll(ItemHandler.globalItemID[i], ItemHandler.globalItemX[i],
								ItemHandler.globalItemY[i], ItemHandler.globalItemAmount[i],
								ItemHandler.globalItemController[i]);
					} else
						Misc.println("Item is static");
				}
	
			}		
			
			
			for (int i = 0; i < NpcHandler.maxNPCs; i++) {
				if (NpcHandler.npcs[i] == null) {
					continue;
				}
				NpcHandler.npcs[i].clearUpdateFlags();
			}          
					
			for (int i = 0; i < NpcHandler.maxNPCs; i++) {
				if (NpcHandler.npcs[i] != null) {					
					if(!NpcHandler.npcs[i].isDead) {
						if(NpcHandler.npcs[i].walkingType != 0) {
							switch(NpcHandler.npcs[i].walkingType) {
								case 5:
									NpcHandler.npcs[i].turnNpc(NpcHandler.npcs[i].getLocation().getX()-1, NpcHandler.npcs[i].getLocation().getY());
								break;
								case 4:
									NpcHandler.npcs[i].turnNpc(NpcHandler.npcs[i].getLocation().getX()+1, NpcHandler.npcs[i].getLocation().getY());
								break;
								case 3:
									NpcHandler.npcs[i].turnNpc(NpcHandler.npcs[i].getLocation().getX(), NpcHandler.npcs[i].getLocation().getY()-1);
								break;
								case 2:
									NpcHandler.npcs[i].turnNpc(NpcHandler.npcs[i].getLocation().getX(), NpcHandler.npcs[i].getLocation().getY()+1);
								break;	
							}
						}
					}
				}
			}
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

}
