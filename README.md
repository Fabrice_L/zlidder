# Zlidder #

Zlidder is a med-performance RuneScape Emulator based on the very first framework named Winterlove.

### What is different next to other servers? ###
The big difference between Zlidder and take for example Project Insanity, is that the server does not exist out of cobbled together tutorials working against eachother.

### How did we improve Winterlove? ###

Winterlove has always been one of the most used frameworks but as everyone knows, it's out of date.
Here at Zlidder, we have taken Winterlove and started breaking features down to the bottom to build them up again using more efficient code.
The plan is to release this server with most standard features done so people can start doing their own magic.

### Contributors ###
* Fabrice L <Skype: mod.page>